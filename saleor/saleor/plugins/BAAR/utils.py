from datetime import datetime
from django.conf import settings
import requests
import json
import pandas as pd
import os
import io
from django.db import connection
import psycopg2
import json
from requests.models import Response
# settings.configure()

# BAAR_host = settings.BAAR_HOST
# BAAR_port = settings.BAAR_PORT
# BAAR_tenant = settings.BAAR_TENANT
# BAAR_user = settings.BAAR_USER
# BAAR_key = settings.BAAR_KEY

# BAAR_host = "18.221.78.83"
# BAAR_port = "81"
# BAAR_tenant = "test1"
# BAAR_user = "admin"
# BAAR_key = "dfd07f597b998eed55b54c966390d345"

BAAR_host = "18.221.163.23"
BAAR_port = "81"
BAAR_tenant = "demo"
BAAR_user = "shourjyag"
BAAR_key = "dfd07f597b998eed55b54c966390d345"


def get_player_wf(wf_name):
    request = requests.get("http://" + BAAR_host + ":" + BAAR_port + "/baar-ws/rest/workflow/WorkflowProcessView?tenant="
                           + BAAR_tenant, headers={"Username": BAAR_user, "Content-Type": "application/json", "API-KEY": BAAR_key, "X-NO-LOGIN-MODE": ""})
    response = request.json()
    for inst in response:
        if inst["workflowProcessModelVo"]["name"].lower() == wf_name.lower():
            return inst["workflowProcessModelVo"]["id"]
    return "Not Found"


def get_servicetask_record():
    request = requests.get("http://" + BAAR_host + ":" + BAAR_port + "/baar-ws/rest/workflow/ServiceTaskClassRecordMethodMappingView?tenant="
                           + BAAR_tenant, headers={"Username": BAAR_user, "Content-Type": "application/json", "API-KEY": BAAR_key, "X-NO-LOGIN-MODE": ""})
    response = request.json()
    if response:
        return response
    return "Unable to get Service Task Class Mapping"


def find_running_wf(wf_id):
    request = requests.get("http://" + BAAR_host + ":" + BAAR_port + "/baar-ws/rest/workflow/WorkflowProcessExecutionView/" + str(wf_id)
                           + "?tenant=" + BAAR_tenant, headers={"Username": BAAR_user, "Content-Type": "application/json", "API-KEY": BAAR_key, "X-NO-LOGIN-MODE": ""})
    response = request.json()
    for inst in response:
        if inst["workflowProcessExecutionModelVo"]["running"] == "true":
            return inst["workflowProcessExecutionModelVo"]["id"]
    return False


def db_query(query, table, db_type):
    query_post = {"query": query, "tenantName": BAAR_tenant,
                  "dbType": db_type, "db": BAAR_tenant + "_baar", "tableName": table}
    request = requests.post("http://" + BAAR_host + ":" + BAAR_port + "/baar-ws/rest/integrator/FetchData?tenant=" + BAAR_tenant, headers={
                            "Username": BAAR_user, "Content-Type": "application/json", "API-KEY": BAAR_key, "X-NO-LOGIN-MODE": ""}, data=json.dumps(wf_post))
    response = request.json()


def start_wf(wf_name):
    wf_id = get_player_wf(wf_name)
    wf_mapping = get_servicetask_record()
    wf_post = {}
    if wf_name and wf_id and wf_mapping:
        wf_post["workflowInstanceId"] = wf_id
        wf_post["variables"] = {}
        wf_post["variables"]["tenant"] = BAAR_tenant
        wf_post["variables"]["workflowName"] = wf_name
        wf_post["variables"]["recordMethodMappingList"] = wf_mapping
        request = requests.post("http://" + BAAR_host + ":" + BAAR_port + "/baar-ws/rest/workflow/WorkflowProcessRun?tenant=" + BAAR_tenant, headers={
                                "Username": BAAR_user, "Content-Type": "application/json", "API-KEY": BAAR_key, "X-NO-LOGIN-MODE": ""}, data=json.dumps(wf_post))
        print(request.text)
        return request.text


def stop_wf(wf_name):
    wf_id = get_player_wf(wf_name)
    status = find_running_wf(wf_id)
    if status != False:
        request = requests.get("http://" + BAAR_host + ":" + BAAR_port + "/baar-ws/rest/workflow/WorkflowProcessExecutionDelete/" + str(status)
                               + "?tenant=" + BAAR_tenant, headers={"Username": BAAR_user, "Content-Type": "application/json", "API-KEY": BAAR_key, "X-NO-LOGIN-MODE": ""})
        response = request.text
        if response == "true":
            return "Successfully Stopped Workflow"
        else:
            return "Unable to Stop Workflow"
    return "Workflow is not running or not found"


def get_component_instance(name):
    request = requests.get("http://" + BAAR_host + ":" + BAAR_port + "/baar-ws/rest/component/ComponentDataView?tenant=" + BAAR_tenant,
                           headers={"Username": BAAR_user, "Content-Type": "application/json", "API-KEY": BAAR_key, "X-NO-LOGIN-MODE": ""})
    response = request.json()
    for instance in response:
        if instance["instanceName"] == name:
            # instance["activeFlag"] = "true"
            # for i in range(len(instance["componentInstanceMappingList"])):
            #     instance["componentInstanceMappingList"][i]["activeFlag"] = "true"
            return instance
    return "Not Found"


def get_component_data(instance_id):
    request = requests.get("http://" + BAAR_host + ":" + BAAR_port + "/baar-ws/rest/component/ComponentDataViewById/" + str(instance_id)
                           + "?tenant=" + BAAR_tenant, headers={"Username": BAAR_user, "Content-Type": "application/json", "API-KEY": BAAR_key, "X-NO-LOGIN-MODE": ""})
    response = request.json()
    return response


def get_wf_id(name):
    request = requests.get("http://" + BAAR_host + ":" + BAAR_port + "/baar-ws/rest/workflow/WorkflowView?tenant=" + BAAR_tenant,
                           headers={"Username": BAAR_user, "Content-Type": "application/json", "API-KEY": BAAR_key, "X-NO-LOGIN-MODE": ""})
    response = request.json()
    for wf in response:
        print(wf)
        if wf["workflowModelVo"]["name"].lower() == name.lower():
            return wf["workflowModelVo"]["id"]
            break
    return "Not Found"


def clear_component(instance):
    instance["componentInstanceInventoryList"] = []
    instance = str(instance).replace("'", '"')
    request = requests.post("http://" + BAAR_host + ":" + BAAR_port + "/baar-ws/rest/component/ComponentDataSave?tenant=" + BAAR_tenant,
                            headers={"Username": BAAR_user, "Content-Type": "application/json", "API-KEY": BAAR_key, "X-NO-LOGIN-MODE": ""}, data=instance)
    response = request.text
    print(response)


def generate_component(file):
    if isinstance(file, pd.DataFrame):
        data = file
    else:
        data = pd.read_excel(file)
    component_list = []
    count = 1
    for index, row in data.iterrows():
        row_dict = {}
        row_dict["rowId"] = count
        row_dict["componentInstanceInventoryMap"] = {}
        for column in row.iteritems():
            row_dict["componentInstanceInventoryMap"][column[0]] = {}
            row_dict["componentInstanceInventoryMap"][column[0]
                                                      ]["inventoryParamName"] = column[0]
            row_dict["componentInstanceInventoryMap"][column[0]
                                                      ]["inventoryParamRowid"] = count
            row_dict["componentInstanceInventoryMap"][column[0]
                                                      ]["inventoryParamValue"] = column[1]
            row_dict["componentInstanceInventoryMap"][column[0]
                                                      ]["inventoryParamDisplayName"] = "null"
            row_dict["componentInstanceInventoryMap"][column[0]]["activeFlag"] = "Y"
        count = count + 1
        # print("ACTUAL COMPONENT VALUE",component_list)
        component_list.append(row_dict)
    return component_list 


def update_component(name):
    if name.FILES:
        print("IF111111____")
        instance_id = get_component_instance(list(name.FILES)[0])["instanceId"]
        
    else:
        print("THIS IS ELSE 1____",name)
        print("NAME_BODY____",name.body)
        json_data = json.loads(str(name.body, encoding='utf-8'))
        table_name = name.path.split("/")[-1]
        instance_id = get_component_instance(table_name)["instanceId"]
        print("UPDATE ID_______",instance_id)
        print("JSON_DATA___",json_data)
        print("TABLE NAME__",table_name)
    data_instance = get_component_data(instance_id)
    clear_component(data_instance)
    if name.FILES:
        print("IF222222____")
        data = generate_component(name.FILES[list(name.FILES)[0]])

    else:
        print("THIS IS ELSE 2_____")
        dataframe = pd.json_normalize(json_data)
        dataframe = dataframe.drop('id', axis=1)
        print("data frame_____",dataframe)
        data = generate_component(dataframe)
        print("DATABASE STORE BAAR",data)
    data_instance["componentInstanceInventoryList"] = data
    data_instance = str(data_instance).replace("'", '"')
    request = requests.post("http://" + BAAR_host + ":" + BAAR_port + "/baar-ws/rest/component/ComponentDataSave?tenant=" + BAAR_tenant,
                            headers={"Username": BAAR_user, "Content-Type": "application/json", "API-KEY": BAAR_key, "X-NO-LOGIN-MODE": ""}, data=data_instance)
    response = request.text
    print(response)
    return response


def generate_component_template(component):
    column = []
    instance = get_component_instance(component)
    for row in instance["componentInstanceMappingList"]:
        column.append(row["paramName"])
    column = list(dict.fromkeys(column))
    template = pd.DataFrame(columns=column)
    path = os.path.join(os.getcwd(), "templates\\component",
                        component.replace(" ", "-") + ".xlsx")
    template.to_excel(path, index=False, header=True)


def get_component_table(name):
    id = get_wf_id(name)
    if id != "Not Found":
        request = requests.get("http://" + BAAR_host + ":" + BAAR_port + "/baar-ws/rest/workflow/WorkflowComponent/" + str(id) + "?tenant="
                               + BAAR_tenant, headers={"Username": BAAR_user, "Content-Type": "application/json", "API-KEY": BAAR_key, "X-NO-LOGIN-MODE": ""})
        component_table = request.json()
        component_json = {}
        component_json["count"] = len(component_table)
        component_json["table"] = []
        for c in component_table:
            component_object = {}
            component_object[c] = []
            col_names = []
            instance = get_component_instance(c)
            component_data = get_component_data(instance["instanceId"])
            for row in instance["componentInstanceMappingList"]:
                col_names.append(row["paramName"])
            col_names = list(dict.fromkeys(col_names))
            print("COLUMNS NAMES--------------------------++++",col_names)
            if component_data["componentInstanceInventoryList"]:
                for entry in component_data["componentInstanceInventoryList"]:
                    row_dict = {}
                    row_dict["id"] = str(entry["rowId"])
                    for key, value in entry["componentInstanceInventoryMap"].items():
                        row_dict[key] = value["inventoryParamValue"]
                    for col in col_names:
                        if col not in row_dict.keys():
                            row_dict[col] = ""
                    component_object[c].append(row_dict)
            else:
                row_dict = {}
                row_dict["id"] = "1"
                for col in col_names:
                    if col not in row_dict.keys():
                        row_dict[col] = ""
                component_object[c].append(row_dict)

            component_json["table"].append(component_object)

            generate_component_template(c)

        print(json.loads(json.dumps(component_json)))
        return json.loads(json.dumps(component_json))


def get_component_headers(wf):
    id = get_wf_id(wf)
    if id != "Not Found":
        request = requests.get("http://" + BAAR_host + ":" + BAAR_port + "/baar-ws/rest/workflow/WorkflowComponent/" + str(id) + "?tenant="
                               + BAAR_tenant, headers={"Username": BAAR_user, "Content-Type": "application/json", "API-KEY": BAAR_key, "X-NO-LOGIN-MODE": ""})
        component_table = request.json()
        component_json = {}
        component_json["count"] = len(component_table)
        component_json["table"] = []
        for c in component_table:
            component_object = {}
            col_names = []
            instance = get_component_instance(c)
            component_data = get_component_data(instance["instanceId"])
            for row in instance["componentInstanceMappingList"]:
                col_names.append(row["paramName"])
            col_names = list(dict.fromkeys(col_names))
            component_object[c] = col_names
            component_json["table"].append(component_object)
    return json.loads(json.dumps(component_json))


def get_data(component):
    col_names = []
    json_array = []
    instance = get_component_instance(component)
    component_data = get_component_data(instance["instanceId"])
    print('component_data_____________',component_data)
    for row in instance["componentInstanceMappingList"]:
        col_names.append(row["paramName"])
    col_names = list(dict.fromkeys(col_names))
    print('col_names_______',col_names)
    if component_data["componentInstanceInventoryList"]:
        for entry in component_data["componentInstanceInventoryList"]:
            row_dict = {}
            row_dict["id"] = str(entry["rowId"])
            for key, value in entry["componentInstanceInventoryMap"].items():
                row_dict[key] = value["inventoryParamValue"]
                print('row value pairs',row_dict)
            for col in col_names:
                if col not in row_dict.keys():
                    row_dict[col] = ""
            json_array.append(row_dict)
            print('ROW APPEND________',json_array)
    else:
        row_dict = {}  
        row_dict["id"] = "1"
        for col in col_names:
            if col not in row_dict.keys():
                row_dict[col] = ""
        json_array.append(row_dict)
    print("GET DATA JSON",json.loads(json.dumps(json_array)))
    return json.loads(json.dumps(json_array))


def generate_download_template(name):
    path = os.path.join(os.getcwd(), "templates\\component",
                        name.replace(" ", "-") + ".xlsx")
    with open(path, "rb") as excel:
        data = excel.read()
    return data


def get_wf_component(wf_id):
    request = requests.get("http://" + BAAR_host + ":" + BAAR_port + "/baar-ws/rest/workflow/WorkflowComponent/" + str(wf_id) + "?tenant="
                           + BAAR_tenant, headers={"Username": BAAR_user, "Content-Type": "application/json", "API-KEY": BAAR_key, "X-NO-LOGIN-MODE": ""})
    response = request.json()
    return response

def start_tasc(request):
    employee_id = json.loads(str(request.body, encoding='utf-8'))["officeid"]
    post_body = {"tenant":"demo","wfProcessId": "process:27:2182830","workflowParam":{"requestDetailList":[{"employeed_id":employee_id}]}}
    request = requests.post("http://" + BAAR_host + ":" + BAAR_port + "/baar-ws/rest/virtualagent/request?tenant=" + BAAR_tenant,
                            headers={"Username": BAAR_user, "Content-Type": "application/json", "API-KEY": BAAR_key, "X-NO-LOGIN-MODE": ""}, data=json.dumps(post_body))
    response = request.text
    print(response)

if __name__ == "__main__":
    print(get_data("Phishing email list"))
    # generate_component_template("phishing email admin configuration")
    # get_component_table("Phishing Email")
    # instance_id = get_component_instance("PhishingEmail")["instanceId"]
    # data_instance = get_component_data(instance_id)
    # clear_component(data_instance)
