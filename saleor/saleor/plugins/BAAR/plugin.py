from typing import Any, Optional
from uuid import uuid4
import requests

from django.core.files.base import ContentFile
from django.conf import settings
from django.core.handlers.wsgi import WSGIRequest
from django.http import HttpResponse, HttpResponseNotFound, JsonResponse, response
from ...core import JobStatus
from ..base_plugin import BasePlugin
from .utils import *
from rest_framework_api_key.models import APIKey


class BAARPlugin(BasePlugin):
    PLUGIN_ID = "baar"
    PLUGIN_NAME = "BAAR"
    DEFAULT_ACTIVE = True
    PLUGIN_DESCRIPTION = "Built-in saleor plugin that handles invoice creation."

    def webhook(self, request: WSGIRequest, path: str, previous_value) -> HttpResponse:
        print(request)
        if request.method == "OPTIONS":
            allowed_methods = ['get', 'post', 'put', 'delete', 'options']
            response = HttpResponse()
            response['Access-Control-Allow-Origin'] = '*'
            response['Access-Control-Allow-Headers'] = '*'
            response['Access-Control-Allow-Methods'] = ','.join(allowed_methods)
            return response
        else:
            try:
                request_key = request.META["HTTP_X_API_KEY"]
                api_key = APIKey.objects.get_from_key(request_key)
                if path == "/start_tasc":
                    status_code = start_tasc(request)
                elif "/start" in path:
                    print(path.split("/")[-1])
                    status_code = start_wf(path.split("/")[-1])
                elif "/stop" in path:
                    status_code = stop_wf(path.split("/")[-1])
                elif path == "/update":
                    status_code = update_component(request)
                elif "/download" in path:
                    content = generate_download_template(path.split("/")[-1])
                    response = HttpResponse(
                        content, content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
                    response['Content-Disposition'] = 'attachment; filename=%s.xlsx' % path.split(
                        "/")[-1]
                    response['Access-Control-Allow-Origin'] = '*'
                    response['Access-Control-Allow-Methods'] = 'GET'
                    return response
                elif "/components" in path:
                    response = JsonResponse(data=get_component_table(
                        path.split("/")[-1].replace("_", " ")))
                    response['Access-Control-Allow-Origin'] = '*'
                    response['Access-Control-Allow-Methods'] = 'GET'
                    return response
                elif "/get_data" in path:
                    response = JsonResponse(data=get_data(
                        path.split("/")[-1].replace("_", " ")), safe=False)
                    response['Access-Control-Allow-Origin'] = '*'
                    response['Access-Control-Allow-Methods'] = 'GET'
                    return response
                elif "/component_headers" in path:
                    response = JsonResponse(data=get_component_headers(
                        path.split("/")[-1].replace("_", " ")))
                    response['Access-Control-Allow-Origin'] = '*'
                    response['Access-Control-Allow-Methods'] = 'GET'
                    return response
                elif "/submit" in path:
                    status_code = update_component(request)

                response = JsonResponse(data={"status": status_code})
                response['Access-Control-Allow-Origin'] = '*'
                response['Access-Control-Allow-Methods'] = 'GET'
                return response
            except:
                response = JsonResponse(data={"status": "Unathuorized"})
                response['Access-Control-Allow-Origin'] = '*'
                response['Access-Control-Allow-Methods'] = 'GET'
                return response
