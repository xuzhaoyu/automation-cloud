// @ts-ignore
import React,{useState} from "react";
import { useInView } from "react-intersection-observer";

import { Icon } from "@components/atoms";
import YoutubeEmbed from "../../../../components/Youtube/YoutubeEmbed";
import { CachedImage } from "@components/molecules";
import play from "../../../../images/play-button.png"


import * as S from "./styles";
import { IProps } from "./types";
import "./scss/index.css"


const MINIMAL_NUMBER_OF_IMAGES_FOR_BUTTONS = 4;

export const ProductGallery: React.FC<IProps> = ({ images }: IProps) => {
  const [imageIndex, setImageIndex] = React.useState<number>(0);
  const [video, setVideo] = useState(false)
  const [active,setActive] =useState(-1)

  const displayButtons = images.length > MINIMAL_NUMBER_OF_IMAGES_FOR_BUTTONS;

  React.useEffect(() => {
    if (imageIndex >= images.length) {
      setImageIndex(0);
    }
  }, [images]);
  console.log("MY VIDEO",video);
  
  const bottomImageRef = React.useRef<HTMLDivElement | null>(null);
  const topImageRef = React.useRef<HTMLDivElement | null>(null);
  const [topImageIntersectionObserver, topImageInView] = useInView({
    threshold: 0.5,
  });

  const [bottomImageIntersectionObserver, bottomImageInView] = useInView({
    threshold: 0.5,
  });

  const setBottomRef = React.useCallback(
    node => {
      bottomImageRef.current = node;
      bottomImageIntersectionObserver(node);
    },
    [bottomImageIntersectionObserver]
  );

  const setTopRef = React.useCallback(
    node => {
      topImageRef.current = node;
      topImageIntersectionObserver(node);
    },
    [topImageIntersectionObserver]
  );

  const setIntersectionObserver = (index: number, lengthOfArray: number) => {
    if (lengthOfArray > MINIMAL_NUMBER_OF_IMAGES_FOR_BUTTONS) {
      if (index === 0) {
        return setTopRef;
      }
      if (index === lengthOfArray - 1) {
        return setBottomRef;
      }
    }
  };
  const  handleClick=(index,event)=>{
    console.log("Border event",event[index].url);
    
    setImageIndex(index)
    setVideo(false)
  }

  const changeHandler=(index)=>{
    // setImageIndex(index)
    setVideo(false)
    if(video === false){
      setImageIndex(index)
    }
  }

  
  console.log("hooray",images.length);
  return (
    <S.Wrapper data-test="productPhotosGallery">
      <S.ThumbnailsContainer>
        {!topImageInView && displayButtons && (
          <S.TopButton
            onClick={() => {
              if (topImageRef.current) {
                topImageRef.current.scrollIntoView({
                  behavior: "smooth",
                  block: "end",
                  inline: "nearest",
                });
              }
            }}
          >
            <Icon name="select_arrow" size={10} />
          </S.TopButton>
        )}
        {!bottomImageInView && displayButtons && (
          <S.BottomButton
            onClick={() => {
              if (bottomImageRef.current) {
                bottomImageRef.current.scrollIntoView({
                  behavior: "smooth",
                  block: "end",
                  inline: "nearest",
                });
              }
            }}
          >
            <Icon name="select_arrow" size={10} />
          </S.BottomButton>
        )}
        <S.ThumbnailList>
          
          
          <ul>
            
            {images &&
              images.length > 0 &&
              images.map((image, index,event) => {
                return (
                  <li
                    key={index}
                    data-test="galleryThumbnail"
                    data-test-id={index}
                    onClick={()=>setActive(index)}
                    onMouseEnter={()=>setActive(index)}
                  >
                    <div >
                      
                    <S.Thumbnail
                      ref={setIntersectionObserver(index, images.length)}
                      onClick={() =>handleClick(index,event)}
                      className={(index===active)?"styled-border":''}
                      // onClick={()=>setVideo(false)}
                      onMouseEnter={() => changeHandler(index)}
                      // activeThumbnail={Boolean(index === imageIndex )}
                    >
                      <CachedImage alt={image.alt}  url={image.url} />
                      
                    </S.Thumbnail>
                    
                    </div>
                  </li>
                );
              })}
              {/* style={{borderWidth:"4px",borderStyle:"solid",borderColor:"black"}} */}
              {/* <p>Video Not Found 404</p> */}
              <div onClick={()=>setActive(100)}
               onMouseEnter={() => setActive(100)} >
                
              <S.Thumbnail
               className={(active === 100 ) ? "styled-border": ''}
              // activeThumbnail={Boolean(true)}
              onClick={()=>setVideo(true)}
              onMouseEnter={() => setVideo(true)}>
                
              <img style={{height:"25px",width:"25px",marginTop:"20px",marginLeft:"3px",borderColor:"white"}}  src={play} />
              
              
              </S.Thumbnail>
              
              </div>
              </ul>
          
        </S.ThumbnailList>
      </S.ThumbnailsContainer>

      <S.Preview data-test="imagePreview">
      {/* {video ===true ?  <YoutubeEmbed  embedId="rokGy0huYEA"/> : 
          <CachedImage 
            alt={images[imageIndex].alt}
            url={images[imageIndex].url}
          />
           } */}
        {video ===true ?  <YoutubeEmbed  embedId="b0nlMkKVm7Y"/> :
        (images && images.length > 0 && imageIndex < images.length && (
          <CachedImage 
            alt={images[imageIndex].alt}
            url={images[imageIndex].url}
          />
        )) }
        {images.length === 0 && <CachedImage />}
        
      </S.Preview>
    </S.Wrapper>
  );
};
