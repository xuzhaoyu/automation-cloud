import React from "react";
import Dialog from "@material-ui/core/Dialog";
// import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";
import TextField from "@material-ui/core/TextField";
import { TransitionProps } from "@material-ui/core/transitions";
import Button from "@material-ui/core/Button";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & { children?: React.ReactElement<any, any> },
  ref: React.Ref<unknown>
) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      "& > *": {
        margin: theme.spacing(1),
        width: "40ch",
        display: "flex",
        flexDirection: "row",
        // marginLeft: "18px",
      },
    },
  })
);

export default function PopupTestRobot({ open,name, setOpen }) {
  const classes = useStyles();
 
   const submitHandler=(e)=>{
    e.preventDefault();
    alert("Submitted");
   }

  return (
    <div>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={() => setOpen(false)}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">
          {"Trial Version for the Workflow"}
        </DialogTitle>
        <DialogContent>
          {console.log("test robot name",name)}
          <DialogContentText id="alert-dialog-slide-description">
            <form onSubmit={submitHandler} className={classes.root} noValidate autoComplete="on">
              <TextField
                type="text"
                id="filled-basic"
                name="First Name"
                label="First Name"
                variant="filled"
                fullWidth={true}
                required
              />

              <TextField
                type="text"
                id="filled-basic"
                name="Last Name"
                label="Last Name"
                variant="filled"
                fullWidth={true}
                required
              />

              <TextField
                type="email"
                id="filled-basic"
                name="Official Email Address"
                label="Official Email Address"
                variant="filled"
                fullWidth={true}
                required
              />

              <TextField
                type="text"
                id="filled-basic"
                name="Company Name"
                label="Company Name"
                variant="filled"
                fullWidth={true}
                required
              />

              <TextField
                type="text"
                id="filled-basic"
                name="Robot to be used"
                label="Robot to be used"
                defaultValue={name}
                variant="filled"
                fullWidth={true}
                required
              />
              <Button
                className="submit-test-robot"
                type="submit"
                variant="contained"
              >
                Submit
              </Button>
            </form>
          </DialogContentText>
        </DialogContent>
        {/* <DialogActions>
          <Button onClick={handleClose} color="primary">
            Disagree
          </Button>
          <Button onClick={handleClose} color="primary">
            Agree
          </Button>
        </DialogActions> */}
      </Dialog>
    </div>
  );
}
