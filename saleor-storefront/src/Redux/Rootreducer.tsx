// import {combineReducers} from "redux";
// import ​{ Tablereducer }​ from "../components/Test/Reducer/Tablereducer";


// const rootReducer=combineReducers({
//  Array:Tablereducer,
// })

// export default rootReducer;

import {combineReducers} from "redux";
import incReducer from "../IncrementReducer/Increment";
import { Tablereducer } from "../components/Test/Reducer/Tablereducer";

const rootReducer=combineReducers({
    increment:incReducer,
    Array:Tablereducer,
})
// export type RootState = ReturnType<typeof rootReducer>
export default rootReducer;