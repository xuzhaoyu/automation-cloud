import React from "react";
import { FormattedMessage } from "react-intl";

import * as S from "./styles";
import { IProps } from "./types";

import { Container } from "../Container";

/**
 * Template for empty cart page.
 */
const CartEmpty: React.FC<IProps> = ({ button }: IProps) => {
  return (
    <Container>
      <S.Wrapper>
        <S.TitleFirstLine>
          <FormattedMessage defaultMessage="Emptied" />
        </S.TitleFirstLine>
        <S.TitleFirstLine>
          <FormattedMessage defaultMessage="Automation" />
        </S.TitleFirstLine>
        <S.HR />
        <S.Subtitle>
          <FormattedMessage defaultMessage=" Choose your Correct Automations" />
        </S.Subtitle>
        <S.ContinueButton>{button}</S.ContinueButton>
      </S.Wrapper>
    </Container>
  );
};

export { CartEmpty };
