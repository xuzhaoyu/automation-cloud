import LinearProgress from "@material-ui/core/LinearProgress";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
// import DialogContentText from "@material-ui/core/DialogContentText";
// import DialogTitle from "@material-ui/core/DialogTitle";
import Typography from "@material-ui/core/Typography";

import PropTypes from "prop-types";

import React, { useState } from "react";

const handleClose = loaderforTable => {
  if (loaderforTable && loaderforTable === false) {
    return false;
  }
};

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
      root: {
        "& > *": {
          margin: theme.spacing(1),
          // width: '25ch',
          width: "38ch",
          textAlign:"center"
        //   flexDirection: "row",
        //   display: "flex",
        },
      },
      closeButton:{
        color: theme.palette.grey[500],
      }
    })
  );

function Loader({ loader, loaderforTable }) {
const classes=useStyles();

  return (
    <div >
      <Dialog
        open={loader}
        onClose={() => handleClose(loaderforTable)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent>
          <div className={classes.root}>
            <Typography className="text-20 mb-16" color="textSecondary">
              Loading...
            </Typography>
            <LinearProgress className="w-xs" color="secondary" />
          </div>
        </DialogContent>
      </Dialog>
    </div>
  );
}

export default Loader;
