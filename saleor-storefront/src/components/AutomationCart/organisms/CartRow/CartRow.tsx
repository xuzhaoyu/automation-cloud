import React, { useState, useEffect } from "react";
import { FormattedMessage, useIntl } from "react-intl";
// import { Link } from "react-router-dom";
// import { useHistory } from "react-router-dom";
// import { Row, Col } from "react-bootstrap";
// import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import AddIcon from "@material-ui/icons/Add";
import {
  DynTable,
  SetLoaderforTable,
  StartBaar,
  StopBaar,
} from "../../../Test/Action/DynTable";
import Automationtable from "../../../Test/Automationtable";
import Spinner from "react-bootstrap/Spinner";
import Loader from "./Loader";
// import { Icon, IconButton } from "@components/atoms";
import { CachedImage, TextField } from "@components/molecules";
// import { commonMessages } from "@temp/intl";
// import { BaarComponent } from "../CartRow/ComponentbaarAction/BaarComponentaction";

// import { generateProductUrl } from "../../../../core/utils";

import { generateAutomationUrl, generateBAARUrl } from "../../../../core/utils";

import * as S from "./styles";
import { IProps } from "./types";
import { Button } from "react-bootstrap";
// import Test1 from "../../../Test/Test1";
// import { SlideDown } from "react-slidedown";
// import {SetLoader} from "../../../Test/Action/DynTable"
import "react-slidedown/lib/slidedown.css";

/**
 * Product row displayed on cart page
 */
interface increment {
  type: any;
}

export const CartRow: React.FC<IProps> = ({
  // index,
  name,
  thumbnail,
  sku,
  id,
}: IProps) => {
  const [open, setOpen] = useState(false);
  const [counter, setCounter] = useState(0);
  const [table, setTable] = useState([]);
  const [loader, setLoader] = useState(false);
  const intl = useIntl();

  const dispatch = useDispatch();
  const tableArray = useSelector((state: any) => state.Array.tableArray);
  const productName = useSelector((state: any) => state.Array.productName);
  const loaderforTable = useSelector((state: any) => state.Array.loaderforTable);
  console.log("NEW TABLE ARRAY ____", tableArray);

  // useEffect(() => {
  //   dispatch(SetLoader());
  // }, []);

  const startButton={
    margin:"0px 4px 0px",
    width:"7.5rem",
    backgroundColor:"#F8931D",
    borderColor:"#F8931D",
    color:"white"
  }
  const stopButton={
    backgroundColor:"#418ACE",
    width:"7.5rem",
    borderColor:"#418ACE",
    color:"white"
  }

  const handleChange1 = name => {
    setLoader(true);
    dispatch(SetLoaderforTable());
    const baar = generateBAARUrl(name);

    dispatch(DynTable(baar, name));
    // dispatch(BaarComponent(baar));
    setOpen(!open);
  };
  console.log("mandown", counter, table);

  const baar12 = generateBAARUrl(name);

  const handleClick1 = baar12 => {
    dispatch(StartBaar(baar12));
  };

  const handleClick2 = baar12 => {
    dispatch(StopBaar(baar12));
  };


  return (
    <>
    <div style={{backgroundColor:"#F7F7F7" ,borderRadius:"4px"}}>
      <S.Wrapper data-test="cartRow" data-test-id={sku}>
        <S.Photo>
          <CachedImage data-test="itemImage" {...thumbnail} />
        </S.Photo>
        <S.Description>
          <AddIcon
            style={{ position: "relative", top: "-25px", left: "-109px" }}
            onClick={() => handleChange1(name)}
          />
          {/* <p>{baar12}</p> */}
          <S.Name style={{color:"#34B4E5"}} data-test="itemName">{name}</S.Name>
        </S.Description>

        <S.Trash>
          <Button
            style={startButton}
            onClick={() => handleClick1(baar12)}
            // variant="primary"
          >
            Start
          </Button>

          <Button onClick={() => handleClick2(baar12)} style={stopButton}>
            Stop
          </Button>
        </S.Trash>
      </S.Wrapper>
      {/* Dynamic tables */}
      {console.log("tableArray___", tableArray)}
      {console.log("LOADER AUTOMATION", loaderforTable)}

      {loaderforTable ? (
        <Loader loader={loader}  loaderforTable={loaderforTable}/>
        // <div style={{justifyContent:"center"}}>
        // <Spinner  animation="border" variant="warning" />
        // </div>
      ) : (
        tableArray.length > 0 &&
        productName &&
        productName === name && <Automationtable />
      )}

       {/* {
        tableArray.length > 0 &&
        productName &&
        productName === name && !loaderforTable &&  <Automationtable />
      }
       {
        tableArray.length > 0 &&
        productName &&
        productName === name && loaderforTable &&  <Spinner animation="border" />
      } */}
      {/* {tableArray.length > 0 && productName && productName === name && 
       <Automationtable /> 
      } */}
      </div>
    </>
  );
};
