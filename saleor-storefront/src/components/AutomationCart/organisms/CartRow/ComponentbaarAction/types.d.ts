interface IBaar {
    count: string
    table:array
  }
  
  type BaarState = {
    articles: IBaar[]
  }
  
  type BaarAction = {
    type: string
    article: IBaar
  }
  
  type DispatchType = (args: BaarAction) => BaarAction