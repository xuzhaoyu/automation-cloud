import * as actionTypes from "./index"

const initialState ={
    counter:"",
    table:[]
}
// interface BAAR_COMPONENT {
//     type:
// }

 const BaarComponentReducer=(state=initialState,action)=>{
    switch (action.type) {
        case actionTypes.BAAR_COMPONENT:
           return {
               ...state,
               counter:action.payload.counter,
               table:action.payload.table
           } 
           
    
        default:
           return state;
    }
}
export default BaarComponentReducer;