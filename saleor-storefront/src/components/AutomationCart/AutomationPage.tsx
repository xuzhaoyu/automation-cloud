import { useAuth, useCart, useCheckout } from "@saleor/sdk";
import { History } from "history";
import React from "react";
import { FormattedMessage } from "react-intl";
import { useHistory } from "react-router-dom";

import { Button, CartFooter, CartHeader } from "./atoms/index";
import { TaxedMoney } from "@components/containers";
import { CartRow}  from "./organisms/index";
import { Cart, CartEmpty } from "./templates/index";
import { IItems } from "@saleor/sdk/lib/api/Cart/types";
import { UserDetails_me } from "@saleor/sdk/lib/queries/gqlTypes/UserDetails";
import { BASE_URL } from "@temp/core/config";
// import { checkoutMessages } from "@temp/intl";
import { ITaxedMoney } from "@types";

import { IProps } from "./types";

const title = (
  <h1 data-test="cartPageTitle">
    <FormattedMessage defaultMessage="Products Automations" />
  </h1>
);

const getShoppingButton = (history: History) => (
  <Button
    testingContext="cartPageContinueShoppingButton"
    onClick={() => history.push(BASE_URL)}
  >
    <FormattedMessage  defaultMessage="Go to Automations"  />
  </Button>
);

// const getCheckoutButton = (history: History, user?: UserDetails_me | null) => (
//   <Button
//     testingContext="proceedToCheckoutButton"
//     onClick={() => history.push(user ? `/checkout/` : `/login/`)}
//   >
//     <FormattedMessage defaultMessage="PROCEED TO CHECKOUT" />
//   </Button>
// );

const cartHeader = <CartHeader />;

const prepareCartFooter = (
  totalPrice?: ITaxedMoney | null,
  shippingTaxedPrice?: ITaxedMoney | null,
  promoTaxedPrice?: ITaxedMoney | null,
  subtotalPrice?: ITaxedMoney | null
) => (
  <CartFooter
    subtotalPrice={
      <TaxedMoney data-test="subtotalPrice" taxedMoney={subtotalPrice} />
    }
    totalPrice={<TaxedMoney data-test="totalPrice" taxedMoney={totalPrice} />}
    shippingPrice={
      shippingTaxedPrice &&
      shippingTaxedPrice.gross.amount !== 0 && (
        <TaxedMoney data-test="shippingPrice" taxedMoney={shippingTaxedPrice} />
      )
    }
    discountPrice={
      promoTaxedPrice &&
      promoTaxedPrice.gross.amount !== 0 && (
        <TaxedMoney data-test="discountPrice" taxedMoney={promoTaxedPrice} />
      )
    }
  />
);

const generateCart = (
  items: IItems,

) => {
  return items?.map(({ id, variant }, index) => (
    <div style={{marginBottom:"13px"}}>
    <CartRow
      key={id ? `id-${id}` : `idx-${index}`}
      index={index}
      id={variant?.product?.id || ""}
      name={variant?.product?.name || ""}
      sku={variant.sku}
      thumbnail={{
        ...variant?.product?.thumbnail,
        alt: variant?.product?.thumbnail?.alt || "",
      }}
    />
    </div>
  ));
};

 const CartPage: React.FC<IProps> = ({}: IProps) => {
  const history = useHistory();
  const { user } = useAuth();
  const { checkout } = useCheckout();
  const {
    loaded,
    removeItem,
    updateItem,
    items,
    // totalPrice,
    // subtotalPrice,
    // shippingPrice,
    // discount,
  } = useCart();

  // const shippingTaxedPrice =
  //   checkout?.shippingMethod?.id && shippingPrice
  //     ? {
  //         gross: shippingPrice,
  //         net: shippingPrice,
  //       }
  //     : null;
  // const promoTaxedPrice = discount && {
  //   gross: discount,
  //   net: discount,
  // };

  if (loaded && items?.length) {
    return (
      <Cart
        title={title}
        // button={getCheckoutButton(history, user)}
        cartHeader={cartHeader}
        // cartFooter={prepareCartFooter(
        //   totalPrice,
        //   shippingTaxedPrice,
        //   promoTaxedPrice,
        //   subtotalPrice
        // )}
        cart={items && generateCart(items)}
      />
    );
  }
  return <CartEmpty button={getShoppingButton(history)} />;
};
export default CartPage;
