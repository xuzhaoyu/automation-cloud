import React from "react";
import {
  Dialog,
  DialogTitle,
  DialogContent,
  Typography,
} from "@material-ui/core";
import { createStyles, makeStyles, Theme } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import TextField from "@material-ui/core/TextField";
// import TextareaAutosize from "@material-ui/core/TextareaAutosize";
import CloseIcon from "@material-ui/icons/Close";
// import Controls from "./Controls";
import Button from "@material-ui/core/Button";

interface PopupProps {
  openPopup: boolean;

  setOpenPopup: (arg0: boolean) => void;
}
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      "& > *": {
        margin: theme.spacing(1),
        // width: '25ch',
        width: "48ch",
        flexDirection: "row",
        display: "flex",
      },
    },
    closeButton:{
      color: theme.palette.grey[500],
    }
  })
);
export const Popup = ({ openPopup, setOpenPopup }: PopupProps): JSX.Element => {
  const classes = useStyles();
  return (
    <Dialog style={{ margin: "0px 2rem 0px" }} open={openPopup}>
      <DialogTitle>
        <div style={{ display: "flex", width: "100%" }}>
          <Typography variant="h6" component="div" style={{ flexGrow: 1 }}>
            Automation
          </Typography>
            <IconButton
            className={classes.closeButton}
             onClick={() => {
              setOpenPopup(false);
            }}>
            <CloseIcon />
          </IconButton>
        </div>
      </DialogTitle>
      <DialogContent style={{ overflow: "hidden" }}>
        <div>
          <form className={classes.root} noValidate autoComplete="off">
            <TextField
              type="text"
              id="outlined-basic"
              label="Name"
              variant="filled"
              fullWidth
              autoFocus
            />
            {/* <TextField type="text" id="outlined-basic"  label="Description" variant="filled" /> */}
            {/* <TextareaAutosize
              maxRows={10}
              aria-label="maximum height"
              placeholder="Maximum 10 rows"
              defaultValue="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                  ut labore et dolore magna aliqua."
            /> */}
            <textarea
              id="w3review"
              name="w3review"
              rows="14"
              cols="50"
              placeholder="type your query here..."
            ></textarea>
            <TextField
              type="email"
              id="outlined-basic"
              label="Email Address"
              fullWidth
              variant="filled"
            />
            {/* <Button style={{marginLeft:"22px"}} type="submit" variant="contained" color="primary"> */}
            <Button
              style={{ marginLeft: "30px", display: "flex" }}
              type="submit"
              variant="contained"
              color="primary"
            >
              Submit
            </Button>
          </form>
        </div>

        {/* <button
          onClick={() => {
            setOpenPopup(false);
          }}
        >
          Close Card   
        </button> */}
      </DialogContent>
    </Dialog>
  );
};

export default Popup;
