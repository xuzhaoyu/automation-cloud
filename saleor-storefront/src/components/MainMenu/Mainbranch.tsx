import React from 'react';
import { Redirect } from 'react-router-dom';

const Mainbranch = {
	settings: {
		layout: {
			config: {}
		}
	},
	routes: [
		{
			path: '/about',   
			component: React.lazy(() => import('../About/About'))
		},
		
	],
	
};

export default Mainbranch;
