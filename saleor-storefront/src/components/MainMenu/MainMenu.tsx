import React, { useState } from "react";
import { FormattedMessage } from "react-intl";
import { useDispatch } from "react-redux";
import { commonMessages } from "@temp/intl";
import { useAuth, useCart } from "@saleor/sdk";
import {openNavbar} from '../Test/Action/DynTable';

import Media from "react-media";
import { Link } from "react-router-dom";
import ReactSVG from "react-svg";

import Controls from "./controls/controls";
import Popup from "./controls/Popup";

import {
  MenuDropdown,
  Offline,
  Online,
  OverlayContext,
  OverlayTheme,
  OverlayType,
} from "..";
import * as appPaths from "../../app/routes";
import { maybe } from "../../core/utils";
// import NavDropdown from "./NavDropdown";
import { TypedMainMenuQuery } from "./queries";

// import cartImg from "../../images/cart.svg";
import cartImg from "../../images/cartupdated.png";
import hamburgerHoverImg from "../../images/hamburger-hover.svg";
import hamburgerImg from "../../images/hamburger.svg";
// import {Container} from "react-bootstrap";

import searchImg from "../../images/searchIcon.png";

import userImg from "../../images/userIcon.png";
import {
  mediumScreen,
  smallScreen,
} from "../../globalStyles/scss/variables.scss";
import "./scss/index.scss";
import "./scss/index.css";
// import image1 from "../../images/BAARfinale.jpg"
// import image1 from "../../images/logo-light.png"
import BaarLogo from "../../images/BaarLogoNew.png";
// import { useHistory } from "react-router-dom";

// import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
// import { generateCategoryUrl } from "../../core/utils";
const MainMenu: React.FC = () => {
  const { user, signOut } = useAuth();
  const { items } = useCart();
  const dispatch = useDispatch();
const handlenavbar=()=>{
  dispatch(openNavbar())
}

  const handleSignOut = () => {
    signOut();
  };

  const cartItemsQuantity =
    (items &&
      items.reduce((prevVal, currVal) => prevVal + currVal.quantity, 0)) ||
    0;

  const [openPopup, setOpenPopup] = useState(false);

  // const handleClickOpen = () => {
  //   setOpen(true);
  // };

  // const history=useHistory()
  // const [dropdownOpen, setDropdownOpen] = useState(false);
  // const onMouseEnter=(e)=>{
  //   setDropdownOpen(true);
  // }
  // const onMouseLeave=(e)=>{
  //   setDropdownOpen(false);
  // }

  // const toggle = () => setDropdownOpen(prevState => !prevState);
  window.addEventListener("scroll", function () {
    var header = document.getElementById("header");
    if (window.scrollY > 0) {
      header.classList.add("sticky");
    } else {
      header.classList.remove("sticky");
    }
  });
  return (
    <OverlayContext.Consumer>
      {overlayContext => (
        <nav className="main-menu" id="header">
          <div className="main-menu__center">
            <Link to={appPaths.baseUrl}>
              {/* <ReactSVG path={logoImg} /> */}
              {/* <h3>CONTENT</h3> */}
              <img src={BaarLogo} alt="cannot render" />
            </Link>
          </div>
          <div className="main-menu__left">
            <TypedMainMenuQuery renderOnError displayLoader={false}>
              {({ data }) => {
                const items = maybe(() => data.shop.navigation.main.items, []);

                return (
                  <ul>
                    {/* three headlinks of the categories in mobile mode */}
                    <Media
                      query={{ maxWidth: mediumScreen }}
                      render={() => (
                        <li
                          data-test="toggleSideMenuLink"
                          className="main-menu__hamburger"
                          onClick={() =>
                            overlayContext.show(
                              OverlayType.sideNav,
                              OverlayTheme.left,
                              { data: items }
                            )
                          }
                        >
                          <ReactSVG
                            path={hamburgerImg}
                            className="main-menu__hamburger--icon"
                          />
                          <ReactSVG
                            path={hamburgerHoverImg}
                            className="main-menu__hamburger--hover"
                          />
                        </li>
                      )}
                    />

                    <Media
                      query={{ minWidth: mediumScreen }}
                      render={() =>
                        items.map(item => (
                          <li
                            data-test="mainMenuItem"
                            className="main-menu__item"
                            key={item.id}
                          >
                            <MenuDropdown
                              suffixClass="__rightdown "
                              head={
                                <li className="main-menu__icon main-menu__user--active">
                                  {/* <ReactSVG path={userImg} /> */}
                                  <Link
                                    className="main-menu__links"
                                    to={item.url}
                                  >
                                    {item.name}
                                  </Link>
                                </li>
                              }
                              content={
                                <ul className="main-menu__dropdown">
                                  {item.children.map(subItem => (
                                    <>
                                      {/* {console.log(subItem)
                                } */}
                                      <li data-test="mobileMenuMyAccountLink">
                                        <Link
                                          to={subItem.url}
                                          //  <Link  to={generateCategoryUrl(subItem.category.id,subItem.category.name)}
                                          key={subItem.id}
                                        >
                                          {subItem.name}
                                        </Link>
                                      </li>
                                    </>
                                  ))}
                                </ul>
                              }
                            />
                            {/* <NavDropdown overlay={overlayContext} {...item} /> */}
                          </li>
                        ))
                      }
                    />
                    <a className="static-about-us" href="https://www.alliedmedia.com/about" target="_blank">ABOUT US</a>
                    <Link onClick={()=>handlenavbar()} className="static-associates" to="/tasm/associates/">ASSOCIATES</Link>
                    <Online>
                      <Media
                        query={{ maxWidth: smallScreen }}
                        render={() => (
                          <>
                            {user ? (
                              <MenuDropdown
                                suffixClass="__rightdown"
                                head={
                                  <li className="main-menu__icon main-menu__user--active">
                                    {/* <ReactSVG path={userImg} /> */}
                                    <img
                                      src={userImg}
                                      alt="user-icon"
                                      className="user__icon"
                                    />
                                  </li>
                                }
                                content={
                                  <ul className="main-menu__dropdown">
                                    <li data-test="mobileMenuMyAccountLink">
                                      <Link to={appPaths.accountUrl}>
                                        <FormattedMessage
                                          {...commonMessages.myAccount}
                                        />
                                      </Link>
                                    </li>
                                    <li data-test="mobileMenuOrderHistoryLink">
                                      <Link to={appPaths.orderHistoryUrl}>
                                        <FormattedMessage
                                          {...commonMessages.orderHistory}
                                        />
                                      </Link>
                                    </li>
                                    <li data-test="mobileMenuAddressBookLink">
                                      <Link to={appPaths.addressBookUrl}>
                                        <FormattedMessage
                                          {...commonMessages.addressBook}
                                        />
                                      </Link>
                                    </li>
                                    <li
                                      onClick={handleSignOut}
                                      data-test="mobileMenuLogoutLink"
                                    >
                                      <FormattedMessage
                                        {...commonMessages.logOut}
                                      />
                                    </li>
                                  </ul>
                                }
                              />
                            ) : (
                              <li
                                data-test="mobileMenuLoginLink"
                                className="main-menu__icon"
                                onClick={() =>
                                  overlayContext.show(
                                    OverlayType.login,
                                    OverlayTheme.left
                                  )
                                }
                              >
                                {/* <ReactSVG path={userImg}/> */}
                                <img
                                  src={userImg}
                                  alt="user-icon"
                                  className="user__icon"
                                />
                              </li>
                            )}
                          </>
                        )}
                      />
                    </Online>
                  </ul>
                );
              }}
            </TypedMainMenuQuery>
          </div>

          <div className="main-menu__right">
            <ul>
              <Online>
                <Controls.Button
                  text="Request Robot"
                  variant="outlined"
                  className="automation"
                  onClick={() => setOpenPopup(true)}
                />
                <Popup
                  openPopup={openPopup}
                  setOpenPopup={setOpenPopup}
                ></Popup>

                <Media
                  query={{ minWidth: smallScreen }}
                  render={() => (
                    <>
                      {user ? (
                        <MenuDropdown
                          head={
                            <li className="main-menu__icon main-menu__user--active">
                              {/* <ReactSVG path={userImg} /> */}
                              <img
                                src={userImg}
                                alt="user-icon"
                                className="user__icon"
                              />
                            </li>
                          }
                          content={
                            <ul className="main-menu__dropdown">
                              <li data-test="desktopMenuMyAccountLink">
                                <Link to={appPaths.accountUrl}>
                                  <FormattedMessage
                                    {...commonMessages.myAccount}
                                  />
                                </Link>
                              </li>
                              <li data-test="desktopMenuOrderHistoryLink">
                                <Link to={appPaths.orderHistoryUrl}>
                                  <FormattedMessage
                                    {...commonMessages.orderHistory}
                                  />
                                </Link>
                              </li>
                              <li data-test="desktopMenuAddressBookLink">
                                <Link to={appPaths.addressBookUrl}>
                                  <FormattedMessage
                                    {...commonMessages.addressBook}
                                  />
                                </Link>
                              </li>
                              <li
                                onClick={handleSignOut}
                                data-test="desktopMenuLogoutLink"
                              >
                                <FormattedMessage {...commonMessages.logOut} />
                              </li>
                            </ul>
                          }
                        />
                      ) : (
                        <li
                          data-test="desktopMenuLoginOverlayLink"
                          className="main-menu__icon"
                          onClick={() =>
                            overlayContext.show(
                              OverlayType.login,
                              OverlayTheme.right
                            )
                          }
                        >
                          {/* <ReactSVG path={userImg}/> */}
                          <img
                            src={userImg}
                            alt="user-icon"
                            className="user__icon"
                          />
                        </li>
                      )}
                    </>
                  )}
                />
              </Online>
              <Offline>
                <li className="main-menu__offline">
                  <Media
                    query={{ minWidth: mediumScreen }}
                    render={() => (
                      <span>
                        <FormattedMessage defaultMessage="Offline" />
                      </span>
                    )}
                  />
                </li>
              </Offline>
              <li
                data-test="menuCartOverlayLink"
                className="main-menu__icon main-menu__cart"
                onClick={() => {
                  overlayContext.show(OverlayType.cart, OverlayTheme.right);
                }}
              >
                <img src={cartImg} alt="cart-icon" style={{ width: "2rem" }} />
                {/* <ReactSVG path={cartImg} /> */}
                {cartItemsQuantity > 0 ? (
                  <span className="main-menu__cart__quantity">
                    {cartItemsQuantity}
                  </span>
                ) : null}
              </li>
              <li
                data-test="menuSearchOverlayLink"
                className="main-menu__search"
                onClick={() =>
                  overlayContext.show(OverlayType.search, OverlayTheme.right)
                }
              >
                <Media
                  query={{ minWidth: mediumScreen }}
                // render={() => (
                //   // <span>
                //   //   <FormattedMessage {...commonMessages.search} />
                //   // </span>
                // )}
                />
                {/* <ReactSVG path={searchImg} /> */}
                <img
                  src={searchImg}
                  alt="searchImg"
                  style={{ width: "1.5rem" }}
                />
              </li>
            </ul>
          </div>
        </nav>
      )}
    </OverlayContext.Consumer>
  );
};

export default MainMenu;
