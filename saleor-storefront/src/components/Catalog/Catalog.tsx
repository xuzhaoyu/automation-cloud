import React,{useState} from "react";
// import { FormattedMessage } from "react-intl";
// import { commonMessages } from "@temp/intl";
import { useAuth, useCart } from "@saleor/sdk";
import {Link} from "react-router-dom";

import Media from "react-media";
// import { Link } from "react-router-dom";
// import ReactSVG from "react-svg";

// import Controls from './controls/controls';
// import Popup from "./controls/Popup"

// import {
//   MenuDropdown,
//   Offline,
//   Online,
//   OverlayContext,
//   OverlayTheme,
//   OverlayType,
// } from "..";
// import * as appPaths from "../../app/routes";
// import { maybe } from "../../core/utils";
//  import NavDropdown from "./NavDropdown";
// import { TypedMainMenuQuery } from "./queries";

// import cartImg from "../../images/cart.svg";
// import hamburgerHoverImg from "../../images/hamburger-hover.svg";
// import hamburgerImg from "../../images/hamburger.svg";
// import logoImg from "../../images/logo.svg";
// import searchImg from "../../images/search.svg";
// import userImg from "../../images/user.svg";
// import {
//   mediumScreen,
//   smallScreen,
// } from "../../globalStyles/scss/variables.scss";
//  import "./scss/index.scss";
// import "./scss/index.css";
// import image1 from "../../images/BAARfinale.jpg"        
// import { useHistory } from "react-router-dom";
import {Card} from "react-bootstrap";
import {generateCategoryUrl} from "../../core/utils";
// import { IProps } from "./types";
import classNames from "classnames";
import {
    ProductsList_categories,
    ProductsList_shop,
    // ProductsList_shop_homepageCollection_backgroundImage,
  } from "./gqlTypes/ProductsList";

  

const Catalog: React.FC<{
    categories: ProductsList_categories;
    shop:ProductsList_shop;
  }>=( {categories,shop})=>{
  // const { user, signOut } = useAuth();    
  // const { items } = useCart();

  // const handleSignOut = () => {
  //   signOut();
  // };

  // const cartItemsQuantity =
  //   (items &&
  //     items.reduce((prevVal, currVal) => prevVal + currVal.quantity, 0)) ||
  //   0;

    // const [openPopup, setOpenPopup] = useState(false);

    const categoriesExist = () => {
      return categories && categories.edges && categories.edges.length > 0;
    };
    // const slugUrl = ":slug([a-z-0-9]+)/:id([0-9]+)/";
    // const baseUrl = "/";
    //  const categoryUrl=generateCategoryUrl(id ,name);

    // const handleClickOpen = () => {
    //   setOpen(true);
    // };
   
    // const history=useHistory()

  return (
    
    <div>
      <h3>Catalog Landing Page</h3>
    {categoriesExist() && (
      <div className="home-page__categories">
        <div className="container">
         

          <div style={{padding:"4%"}}>
          
          
            {categories.edges.map(({ node: category }) => (
              
              <div key={category.id}>
                  
                <Link
                  to={generateCategoryUrl(category.id, category.name)}
                  key={category.id}
                >
                  <div
                    // style={{backgroundImage: `url(${category.backgroundImage.url})` }}
                    // style={{
                    //   backgroundImage: `url(${
                    //     category.backgroundImage
                    //       ? category.backgroundImage.url
                    //       : noPhotoImg
                    //   })`,
                    // }}
                  /> 
                  <h3>{category.name}</h3>
                </Link>
              </div> 
            ))}
          </div>
        </div>
      </div>
      )}  
        
        </div>   
        
  );
};

export default Catalog;
