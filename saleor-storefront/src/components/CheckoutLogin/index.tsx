import "./scss/index.scss";
import "./scss/index.css";

import React, { useContext } from "react";
import { Redirect } from "react-router";
import { FormattedMessage } from "react-intl";

import { useAuth } from "@saleor/sdk";

import { Offline, OfflinePlaceholder, Online, OverlayContext } from "..";

// import CheckoutAsGuest from "./CheckoutAsGuest";
import SignInForm from "./SignInForm";
import { OverlayType, OverlayTheme } from "../Overlay";

const CheckoutLogin: React.FC<{}> = () => {
  const overlay = useContext(OverlayContext);
  const { user } = useAuth();
  const { show } = overlay;

  const showPasswordResetOverlay = () => {
    show(OverlayType.password, OverlayTheme.right);
  };

  if (user) {
    return <Redirect to="/checkout/" />;
  }
  return (
    <div className="container">
      <Online>
        <div className="checkout-login">
          {/* <CheckoutAsGuest overlay={overlay} checkoutUrl="/checkout/" /> */}

          <div className="checkout-login__user">
            
            <SignInForm onForgottenPasswordClick={showPasswordResetOverlay} />
            <div className="register__user">
              <p>
                <FormattedMessage defaultMessage="Or you can" />{" "}
                <span
                  data-test="showRegisterOverlay"
                  className="u-link"
                  onClick={() =>
                    overlay.show(OverlayType.register, OverlayTheme.right)
                  }
                >
                  <FormattedMessage defaultMessage="create an account" />
                </span>
              </p>
            </div>
           
          </div>
        </div>
      </Online>
      <Offline>
        <OfflinePlaceholder />
      </Offline>
    </div>
  );
};

export default CheckoutLogin;
