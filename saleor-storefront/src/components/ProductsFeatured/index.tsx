import * as React from "react";
import { Link } from "react-router-dom";

import { Carousel, ProductListItem } from "..";
import { generateProductUrl, maybe } from "../../core/utils";
import { TypedFeaturedProductsQuery } from "./queries";

import "./scss/index.scss";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

interface ProductsFeaturedProps {
  title?: string;
}

const ProductsFeatured: React.FC<ProductsFeaturedProps> = ({ title }) => {
  // var settings = {
  //   dots: false,
  //   infinite: true,
  //   speed: 500,
  //   slidesToShow: 4,
  //   slidesToScroll: 1,
  //   autoplay:false,
  //   mobileFirst:true,
  //   responsive: [
  //     {
  //       breakpoint: 1250,
  //       settings: {
  //         slidesToShow: 3,
  //         slidesToScroll: 1,
  //         infinite: true,
  //         dots: true
  //       }
  //     },
  //     {
  //       breakpoint: 947,
  //       settings: {
  //         slidesToShow: 2,
  //         slidesToScroll: 1
  //       }
  //     },
  //     {
  //       breakpoint: 766,
  //       settings: {
  //         slidesToShow: 1,
  //         slidesToScroll: 1
  //       }
  //     }
  //     // You can unslick at a given breakpoint now by adding:
  //     // settings: "unslick"
  //     // instead of a settings object
  //   ]

  // };
  return (
    <TypedFeaturedProductsQuery displayError={false}>
      {({ data }) => {
        const products = maybe(
          () => data.shop.homepageCollection.products.edges,
          []
        );

        if (products.length) {
          return (
            <div className="products-featured">
              <div className="container">
                <div className="products-featured__header">
                  <h3>{title}</h3>
                  <button>View All</button>
                </div>

                <div className="products-featured__products">
                  {console.log("SHOP BY AUTOMATIONS", products)}

                  {products.map(({ node: product }) => (
                    <Link
                      to={generateProductUrl(product.id, product.name)}
                      key={product.id}
                    >
                      <ProductListItem product={product} />
                    </Link>
                  ))}
                </div>
              </div>
            </div>
          );
        }
        return null;
      }}
    </TypedFeaturedProductsQuery>
    // <TypedFeaturedProductsQuery displayError={false}>
    //   {({ data }) => {
    //     const products = maybe(
    //       () => data.shop.homepageCollection.products.edges,
    //       []
    //     );

    //     if (products.length) {
    //       return (
    //         <div className="products-featured">
    //           <div className="container">
    //             <h3>{title}</h3>
    //             {/* <Carousel> */}
    //             <Slider  {...settings}>

    //               {products.map(({ node: product }) => (
    //                 <Link
    //                   to={generateProductUrl(product.id, product.name)}
    //                   key={product.id}
    //                 >
    //                   <ProductListItem product={product} />
    //                 </Link>

    //               ))}
    //             {/* </Carousel> */}
    //             </Slider>
    //           </div>
    //         </div>
    //       );
    //     }
    //     return null;
    //   }}
    // </TypedFeaturedProductsQuery>
  );
};

ProductsFeatured.defaultProps = {
  title: "Featured",
};

export default ProductsFeatured;
