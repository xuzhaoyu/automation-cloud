import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
// import TextField from "../../components/TextField/index";
// import {nanoid} from "nanoid";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import { useDispatch } from "react-redux";
import { AddFunc } from "./Action/DynTable";
import { AddSubmitData } from "./Action/DynTable";

export default function AddDynamicForm(props) {
  let data1 = {};
  const addStyle = {
    marginBottom: "15px",
    // padding:""
    display: "flex",
    flexDirection: "row" as "row",
    marginTop: "1rem",
  };
  const { tableColumns, tableName, row, openAdd, setOpenAdd } = props;
  // const splicedTableColumns=tableColumns.splice(0,1);

  // console.log("SPLICED COLUMNS",splicedTableColumns);
  const [addFormData, setAddFormData] = useState(data1);
  const [totalNewAdd, setTotalNewAdd] = useState([]);

  const dispatch = useDispatch();

  tableColumns.forEach((d, i) => {
    if (tableColumns) {
      for (var x = 0; x < tableColumns.length; x++) {
        data1[tableColumns[x]] = "";
      }
    }
  });

  console.log("EMPTY OBJECT__________", data1);

  const handleAddFormChange = event => {
    event.preventDefault();
    // setValue(event.target.value);
    const fieldName = event.target.getAttribute("name");
    const fieldValue = event.target.value;

    // const newFormData = { ...addFormData };
    // newFormData[fieldName] = fieldValue;

    setAddFormData({ ...addFormData, [fieldName]: fieldValue });

    console.log("fieldName_____", fieldName);
    console.log("fieldValue______", fieldValue);
  };

  const handleAddFormSubmit = event => {
    event.preventDefault();

    dispatch(AddFunc(addFormData));
    const addAllData = [...row, addFormData];
    setOpenAdd(false);
    dispatch(AddSubmitData(tableName, addAllData));
    console.log("NEWLY ADDED DATA _____", addFormData);
  
  };
  console.log("NEW ADDITION ARRAY", totalNewAdd);

  const isDisable = () => {
    let disAble = true;

    if (addFormData) {
      tableColumns.forEach(column => {
        if (addFormData[column]) {
          console.log("HELLO ISDISABLE FALSE__");
          disAble = false;
          //  return false
        } else {
          console.log("HELLO ISDISABLE TRUE__");
          disAble = true;
        }
      });
    }
    return disAble;
  };
  return (
    <div>
      <Dialog
        open={openAdd}
        onClose={() => setOpenAdd(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Add Your Info"}</DialogTitle>
        <DialogContent>
          <form onSubmit={handleAddFormSubmit}>
            {tableColumns.map((columns, i) => {
              if (columns && columns !== "id") {
                return (
                  <div key={i} style={addStyle}>
                    <TextField
                      // id="outlined-secondary"
                      type="text"
                      multiline
                      maxRows={4}
                      key={i}
                      name={columns}
                      label={`Enter the ${columns}`}
                      variant="outlined"
                      color="primary"
                      autoComplete={columns}
                      onChange={handleAddFormChange}
                      required
                    />
                  </div>
                );
              }
            })}
            <Button
              style={{
                width: "100%",
                
              }}
              type="submit"
              variant="contained"
              color="primary"
              disabled={isDisable()}
            >
              ADD
            </Button>
          </form>
        </DialogContent>
      </Dialog>
    </div>
  );
}
