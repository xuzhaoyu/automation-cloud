import "./scss/index.css";
import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button } from "react-bootstrap";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import Collapse from "@material-ui/core/Collapse";
import IconButton from "@material-ui/core/IconButton";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
// import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
// import Typography from "@material-ui/core/Typography";
// import Paper from "@material-ui/core/Paper";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import EditDynamicForm from "./EditDynamicForm";
// import { DynTable } from "./Action/DynTable";
import AddDynamicForm from "./AddDynamicForm";
import DownloadSheet from "./DownloadSheet";
import UploadNew from "./UploadNew";
import { DynTableNames } from "./Action/DynTable";
import { DeleteSubmitData } from "./Action/DynTable";
import { SetTablempty } from "./Action/DynTable";
import { EditFormdata } from "./Action/DynTable";
import { SetLoader } from "./Action/DynTable";
import Spinner from "react-bootstrap/Spinner";
// import * as Actions from "./Actions/DynTable";
// import Accordion from "@material-ui/core/Accordion";
// import axios from "axios";

const useRowStyles = makeStyles({
  root: {
    "& > *": {
      borderBottom: "unset",
      width:"1000px",
      // margin:"0 auto"
      // backgroundColor:"#F1F1F1"
    },
  },
  tableContent: {
    // padding:"3rem",
    width:"1000px",
    // margin:"0 auto",
    backgroundColor: "#F1F1F1",
  },
});

function Row(props) {
  // const tableArray = useSelector((state:any) => state.Array.tableArray);
  const loader = useSelector((state: any) => state.Array.loader);
  const tableData = useSelector((state: any) => state.Array.tableData);
  const tableArrayName = useSelector((state: any) => state.Array.tableName);
  const ChildData = useSelector((state: any) => state.Array.data1);
  const EditFormdata1 = useSelector((state: any) => state.Array.item1);
  // const NeweditedData = useSelector(state => state.Array.editedData)

  const dispatch = useDispatch();

  let editedData = {};
  const { item, key } = props;
  const [columns, setColumns] = useState([]);
  const [editId, setEditId] = useState(null);
  const [editFormdata, setEditFormdata] = useState(editedData);
  const [openEdit, setOpenEdit] = useState(false);
  // const [editContactId, setEditContactId] = useState(null);
  const [table, setTable] = useState("");
  const [row, setRow] = useState([]);
  const [open, setOpen] = React.useState(false);
  const [close, setClose] = useState(false);
  const [openAdd, setOpenAdd] = useState(false);
  const classes = useRowStyles();

  const editButton = {
    border: "2px solid #92A5B4",
    backgroundColor: "#F1F1F1",
    marginRight: "10px",
    color: "#386488",
  };
  const deleteButton = {
    border: "2px solid #92A5B4",
    backgroundColor: "#F1F1F1",
    color: "#386488",
  };
  const uploadButton = {
    marginLeft: "4px",
    backgroundColor: "#BED1FC",
    color: "#426488",
    borderColor: "#BED1FC",
  };
  console.log("tableArrayName____", tableArrayName);
  console.log("Column data_________", columns);
  console.log("EDITED DATA_________", EditFormdata1);

  //   console.log("tableArrayName++++++____",Object.keys(tableArrayName[0]));

  columns.forEach((item, ind) => {
    for (var i = 0; i <= columns.length; i++) {
      editedData[columns[i]] = "";
    }
  });
  console.log("EMPTY EDITED OBJ_____", editedData);

  useEffect(() => {
    if (ChildData) {
      setRow([...row, ChildData]);
    }
  }, [ChildData]);

  useEffect(() => {
    if (tableData) {
      setColumns(Object.keys(tableArrayName[0]));
      setRow(tableArrayName);
    }

    if (!open) {
      setColumns([]);
      setRow([]);
    }
  }, [tableData, open]);

  // useEffect(() => {
  //   CanbeDisable(Object.keys(item)[0])

  // }, [table])
  console.log("ROW___ID");
  const switchHandler = tableName => {
    dispatch(SetLoader());
    props.onClick(props.open ? "" : tableName);
    if (!props.open) {
      setTable(tableName);
      dispatch(SetTablempty());
      dispatch(DynTableNames(tableName));
      console.log("Data___++++++++++++", tableName);
      console.log("MY TYPE________", typeof tableName);
      setOpen(true);
    }

    if (props.open) {
      setColumns([]);
      setRow([]);
      setOpen(false);
    }
  };

  const CanbeDisable = tableName => {
    if (table === "") {
      return false;
    } else if (table === tableName) {
      return false;
    } else {
      return true;
    }
  };

  const handleEditClick = (event, item1) => {
    event.preventDefault();
    setOpenEdit(true);
    console.log("ITEM+++++++++++++", item1);
    setEditId(item1);
    dispatch(EditFormdata(item1));
    console.log("EDITED__ID", item1.id);
  };

  const handleDeleteClick = deletetId => {
    const newDeleted = [...row];
    const index = row.findIndex(contact => contact.id === deletetId);
    newDeleted.splice(index, 1);
    setRow(newDeleted);
    dispatch(DeleteSubmitData(table, newDeleted));
  };

  // const setEdithandler = () => {
  //   setOpenEdit(true);
  // };

  return (
    <React.Fragment>
      <TableRow key={key} className={classes.root}>
        <TableCell component="th" scope="row">
          {/* component table names */}
          <strong style={{ color: "#325B7C" }}>
            {Object.keys(item)[0].toUpperCase()}
          </strong>

          <IconButton
            aria-label="expand row"
            size="small"
            style={{ color: "orange" }}
            onClick={() => switchHandler(Object.keys(item)[0])}
            disabled={CanbeDisable(Object.keys(item)[0])}
          >
            {props.open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
        {/* <TableCell>
          
        </TableCell> */}
        <TableCell component="th" scope="row">
          {props.open && <DownloadSheet row={row} columns={columns} />}
          {/* <DownloadComponent download={Object.keys(item)[0]} /> */}
        </TableCell>
        <TableCell component="th" scope="row">
          <Button style={uploadButton} onClick={() => setClose(true)}>
            Upload
          </Button>
          <UploadNew
            close={close}
            setClose={setClose}
            upload={Object.keys(item)[0]}
          />
        </TableCell>
      </TableRow>

      <TableRow key={key}>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={props.open} timeout="auto" unmountOnExit>
            {/* <Accordion expanded={expanded === table} onChange={()=>handleChange(table)}> */}
            {loader ? (
              <Spinner animation="border" variant="warning" />
            ) : (
              <Box className={classes.tableContent} margin={1}>
                <div className="table-scroll"> 
                <Table size="small" aria-label="purchases">
                  <TableHead>
                    <TableRow key={key}>
                      {columns.map((item, i) => {
                        if (item !== "id") {
                          return (
                            <TableCell align="center" key={i}>
                              {/* workflow header names */}
                              <strong style={{ color: "#889DB8" }}>
                                {item.toUpperCase()}
                              </strong>
                            </TableCell>
                          );
                        }
                      })}
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {row.map((item1, ind) => (
                      <>
                        <TableRow key={ind}>
                          {columns.map((ditem, i) => {
                            if (ditem !== "id") {
                              if (ditem === "Password") {
                                var passEmpty = "";
                                var passlen = item1[ditem].length;

                                for (var i = 0; i < passlen; i++) {
                                  passEmpty += "*";
                                }
                                return (
                                  <TableCell key={i}>{passEmpty}</TableCell>
                                );
                              } else {
                                return (
                                  <TableCell align="center" key={i}>
                                    {/* workflow table data's */}
                                    {item1[ditem]}
                                  </TableCell>
                                );
                              }
                            }
                          })}
                          <TableCell
                            style={{ display: "flex", flexDirection: "row" }}
                          >
                            <Button
                              style={editButton}
                              onClick={event => handleEditClick(event, item1)}
                            >
                              EDIT
                            </Button>

                            <Button
                              style={deleteButton}
                              onClick={() => handleDeleteClick(item1.id)}
                            >
                              DELETE
                            </Button>
                          </TableCell>
                          {/* <TableCell style={{marginRight:"4rem"}}>
                            
                          </TableCell> */}
                        </TableRow>
                      </>
                    ))}
                  </TableBody>
                  <div style={{ display: "flex", flexDirection: "row" }}>
                    <Button
                      variant="contained"
                      color="primary"
                      style={{
                        marginTop: "8px",
                        marginRight: "22px",
                        border: "2px solid #92A5B4",
                        backgroundColor: "#F1F1F1",
                        color: "#386488",
                      }}
                      onClick={() => setOpenAdd(true)}
                    >
                      ADD
                    </Button>
                    <Button
                      variant="outlined"
                      color="primary"
                      style={{ marginTop: "8px",
                      border: "2px solid #92A5B4",
                      backgroundColor: "#F1F1F1",
                      color: "#386488",
                    }}
                      onClick={() => alert("Submittted Successfully")}
                    >
                      SUBMIT
                    </Button>
                  </div>

                </Table>
                </div>
                <EditDynamicForm
                  openEdit={openEdit}
                  setOpenEdit={setOpenEdit}
                  tableColumns={columns}
                  tableName={table}
                  row={row}
                  setRow={setRow}
                  editId={editId}
                />
                <AddDynamicForm
                  openAdd={openAdd}
                  setOpenAdd={setOpenAdd}
                  row={row}
                  tableName={table}
                  tableColumns={columns}
                />
                {/* <Button variant="contained" color="primary" >Add NEW</Button> */}
              </Box>
            )}
            {/* </Accordion> */}
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

export default function CollapsibleTable() {
  // const [data, setData] = useState([]);
  // const dispatch = useDispatch();
  const [openRow, setOpenRow] = useState("");
  const tableArray = useSelector((state: any) => state.Array.tableArray);

  // useEffect(() => {
  //   dispatch(DynTable());
  // }, []);

  return (
    <Table style={{ width: "1000px" }} aria-label="collapsible table">
      <TableBody>
        {tableArray.map((item, i) => (
          <Row
            key={i}
            open={Object.keys(item)[0] === openRow}
            onClick={name => setOpenRow(name)}
            item={item}
          />
        ))}
      </TableBody>
    </Table>
  );
}
