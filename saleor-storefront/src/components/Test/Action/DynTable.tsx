import * as actionTypes from "./Actiontypes";
import axios from "axios";
// import api from "./api.json"
// import * as fff from "../../../../tsconfig.json"

export const DynTable = (baar, name) => async dispatch => {
  try {
    const res = await axios.get(
      "http://150.230.25.229:9000/plugins/baar/components/" + baar,
      {
        headers: {
          "X-API-KEY": "u073hizT.txgbxT0VL793cyzaL9Kd5OBNtk5UV1f5",
        },
      }
    );
    console.log(res);
    if (res.request.status === 200) {
      console.log("DATA", res.data);
      const data = res.data.table;
      //  setData(res.data.table)
      dispatch({
        type: actionTypes.DYNAMIC__TABLES,
        payload: { dynTab: data, productname: name },
      });
    }

    // .catch((err) => console.log(err));
  } catch (err) {
    console.log(err);
  }
};

// const loadPosts = () => async (dispatch) => { const response = await axios.get(ROOT_URL + '/posts'); return dispatch(updatePosts(response.data)); };

export const DynTableNames = tab => async dispatch => {
  try {
    const res = await axios.get(
      "http://150.230.25.229:9000/plugins/baar/get_data/" + tab,
      {
        headers: {
          "X-API-KEY": "u073hizT.txgbxT0VL793cyzaL9Kd5OBNtk5UV1f5",
        },
      }
    );

    if (res.request.status === 200) {
      console.log("DATA-TABLES", res.data);
      const data = res.data;
      dispatch({
        type: actionTypes.DYNAMIC__TABLE_NAMES,
        payload: { dynName: data, bool: true },
      });
    }
  } catch (err) {
    console.log(err);
  }
};

export const SetLoader = () => {
  return dispatch => {
    dispatch({ type: actionTypes.SET_LOADER_DATA });
  };
};

export const SetLoaderforTable = () => {
  return dispatch => {
    dispatch({ type: actionTypes.SET_LOADER_FOR_TABLE });
  };
};

export const SetTablempty = () => {
  return dispatch => {
    dispatch({ type: actionTypes.SETTABLENAME });
  };
};

export const openNavbar = () => {
  return dispatch => {
    dispatch({ type: actionTypes.OPEN_NAVBAR, });
  };
};

export const closeNavbar = () => {
  return dispatch => {
    dispatch({ type: actionTypes.CLOSE_NAVBAR, });
  };
};

export const ChangetableData = () => {
  return dispatch => {
    dispatch({ type: actionTypes.CHANGE__TABLE__DATA });
  };
};

export const AddFunc = data1 => {
  return dispatch => {
    dispatch({ type: actionTypes.ADD_NEW_DATA, payload: data1 });
  };
};

export const EditFormdata = item1 => {
  return dispatch => {
    dispatch({ type: actionTypes.EDIT_FORM_DATA, payload: item1 });
  };
};

export const EditFunc = editFormdata => {
  return dispatch => {
    dispatch({ type: actionTypes.EDITED_NEW_DATA, payload: editFormdata });
  };
};

export const SetSubmit =(submit)=>{
  return dispatch =>{
  dispatch({type:actionTypes.SET_SUBMIT,payload:submit})
  };
}

export const AddSubmitData = (tableName, data) => async dispatch => {
  try {
    const res = await axios.post(
      "http://150.230.25.229:9000/plugins/baar/submit/" + tableName,
      data,
      {
        headers: {
          "X-API-KEY": "u073hizT.txgbxT0VL793cyzaL9Kd5OBNtk5UV1f5",
        },
      }
    );

    console.log("EDIT RESPONSE", res);
    if (res.request.status === 200) {
      alert("ADDED DATA");
    }
  } catch (err) {
    console.log(err);
  }
};

export const EditSubmitData = (tableName, editFormdata) => async dispatch => {
  try {
    const res = await axios.post(
      "http://150.230.25.229:9000/plugins/baar/submit/" + tableName,
      editFormdata,
      {
        headers: {
          "X-API-KEY": "u073hizT.txgbxT0VL793cyzaL9Kd5OBNtk5UV1f5",
        },
      }
    );

    console.log("EDIT RESPONSE", res);
    if (res.request.status === 200) {
      alert("EDITED DATA");
    }
  } catch (err) {
    console.log(err);
  }
};

export const DeleteSubmitData = (tableName, delFormdata) => async dispatch => {
  try {
    const res = await axios.post(
      "http://150.230.25.229:9000/plugins/baar/submit/" + tableName,
      delFormdata,
      {
        headers: {
          "X-API-KEY": "u073hizT.txgbxT0VL793cyzaL9Kd5OBNtk5UV1f5",
        },
      }
    );

    console.log("DELETE RESPONSE", res);
    if (res.request.status === 200) {
      alert("DELETED DATA");
    }
  } catch (err) {
    console.log(err);
  }
};

export const StartBaar = baar => async dispatch => {
  try {
    const res = await axios.get(
      "http://150.230.25.229:9000/plugins/baar/start" + baar,
      {
        headers: {
          "X-API-KEY": "u073hizT.txgbxT0VL793cyzaL9Kd5OBNtk5UV1f5",
        },
      }
    );

    console.log("Workflow Started" + baar, res);
    if (res.request.status === 200) {
      alert("Workflow Started");
    }
  } catch (err) {
    console.log(err);
  }
};

export const StopBaar = baar => async dispatch => {
  try {
    const res = await axios.get(
      "http://150.230.25.229:9000/plugins/baar/stop" + baar,
      {
        headers: {
          "X-API-KEY": "u073hizT.txgbxT0VL793cyzaL9Kd5OBNtk5UV1f5",
        },
      }
    );

    console.log("Workflow Started for" + baar, res);
    if (res.request.status === 200) {
      alert(res.data.status);
    }
  } catch (err) {
    console.log(err);
  }
};

export const officialData = (data) => async dispatch => {
  try {
    const res = await axios.post(
      "http://150.230.25.229:9000/plugins/baar/start_tasc" ,
      data,
      {
        headers: {
          "X-API-KEY": "u073hizT.txgbxT0VL793cyzaL9Kd5OBNtk5UV1f5",
        },
      }
    );

    console.log("TCWS RESPONSE", res);
    if (res.request.status === 200) {
      console.log("TCWS DATA");
    }
  } catch (err) {
    console.log(err);
  }
};
