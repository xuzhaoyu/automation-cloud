import React from "react";
import { Formik, Form } from "formik";
import { Button } from "react-bootstrap";
import Dialog from "@material-ui/core/Dialog";
import DialogContent from "@material-ui/core/DialogContent";
// import DialogContentText from '@material-ui/core/DialogContentText';

export default function UploadNew({upload,close,setClose}) {

  return (
    <div>
      <Dialog open={close} onClose={() => setClose(false)}>
        <DialogContent>
          {/* <DialogContentText>{tableName}</DialogContentText> */}
          <Formik
            //   key={i}
            initialValues={{ file: "" }}
            onSubmit={values => {
              console.log("FOOTAGE", values);

              let data = new FormData();

              data.append(upload, values.file);

              return fetch("http://18.221.78.83:9000/plugins/baar/update", {
                method: "POST",
                headers: new Headers({
                  "X-API-KEY": "u073hizT.txgbxT0VL793cyzaL9Kd5OBNtk5UV1f5",
                }),
                body: data,
              })
                .then(response => {
                  response.json();
                })
                .then(data => console.log(data))
                .catch(error => console.log(error));
            }}
          >
            {formProps => (
              <Form>
                <input
                  type="file"
                  id="file1"
                  name="file1"
                  onChange={e =>
                    formProps.setFieldValue("file", e.target.files[0])
                  }
                  multiple
                />

                <Button
                  onClick={() => alert("Uploaded Successfully")}
                  style={{ marginRight: "4px"}}
                  type="submit"
                  // variant="outline-success"
                >
                  {" "}
                  Upload
                </Button>
              </Form>
            )}
          </Formik>
        </DialogContent>
      </Dialog>
    </div>
  );
}
