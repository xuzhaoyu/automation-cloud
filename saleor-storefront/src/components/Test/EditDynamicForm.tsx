import React, { useState, useEffect } from "react";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import Button from "@material-ui/core/Button";
// import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import { useSelector, useDispatch } from "react-redux";
import TextField from "@material-ui/core/TextField";
// import {EditFunc} from "./Actions/DynTable";
import { EditSubmitData } from "./Action/DynTable";
// editFormdata, setEditFormdata, item1,

export default function EditDynamicForm(props) {
  const addStyle = {
    marginBottom: "10px",
    display: "flex",
    flexDirection: "row" as "row",
  };

  const EditFormdata = useSelector((state: any) => state.Array.item1);

  const [allEdit, setAllEdit] = useState({});
  const dispatch = useDispatch();
  const {
    row,
    setRow,
    editId,
    tableColumns,
    tableName,
    openEdit,
    setOpenEdit,
  } = props;

  useEffect(() => {
    setAllEdit(EditFormdata);
  }, [EditFormdata]);

  console.log("ALL EDITED NEW DATA______", allEdit);

  const handleEditFormChange = event => {
    event.preventDefault();
    console.log("EVENT NAME", event.target.name);
    console.log("EVENT VALUE", event.target.value);
    setAllEdit({ ...allEdit, [event.target.name]: event.target.value });
    // const fieldName = event.target.getAttribute("name");
    // const fieldValue = event.target.value;

    // const newFormData = { ...editFormdata,[fieldName]:fieldValue };
    // // newFormData[fieldName] = fieldValue;
    // console.log("NEWLY EDITED_______",newFormData);
    // setEditFormdata(newFormData);
  };

  const handleEditFormSubmit = event => {
    event.preventDefault();
    const newContacts = [...row];

    const index = row.findIndex(contact => contact.id === editId.id);

    newContacts[index] = { ...allEdit };
    // dispatch(EditFunc(editFormdata));
    setOpenEdit(false);
    dispatch(EditSubmitData(tableName, newContacts));
    setRow(newContacts);
    // const editedContact = {
    //   id: editContactId,
    //   fullName: editFormData.fullName,
    //   address: editFormData.address,
    //   phoneNumber: editFormData.phoneNumber,
    //   email: editFormData.email,
    // };
    // setContacts(newContacts);
    // setEditContactId(null);
  };

  const isDisable=()=>{
    let disAble=false

    if(allEdit){
     tableColumns.forEach((column)=>{

       if(allEdit[column]){
        console.log("HELLO ISDISABLE FALSE__");
        //  return false
       }
       else{
        console.log("HELLO ISDISABLE TRUE__");
         disAble=true
       }

     })
    }
    return disAble
  }

  return (
    <div>
      <Dialog
        open={openEdit}
        onClose={() => setOpenEdit(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Edit your Info"}</DialogTitle>
        <DialogContent>
          {tableColumns.map((column,ind) => {
            if (column && column !== "id") {
              return (
                <TextField
                  type="text"
                  key={ind}
                  multiline
                  maxRows={4}
                  style={addStyle}
                  variant="outlined"
                  label={`${column}`}
                  color="primary"
                  name={column}
                  value={allEdit[column]}
                  onChange={handleEditFormChange}
                  required
                />
              );
            }
          })}
          <Button
            style={{ width: "100%" }}
            color="primary"
            onClick={event => handleEditFormSubmit(event)}
            disabled={isDisable()}
          >
            Save
          </Button>
        </DialogContent>
      </Dialog>
    </div>
  );
}
