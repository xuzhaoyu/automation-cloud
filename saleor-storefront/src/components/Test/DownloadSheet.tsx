import React from "react";
import ReactExport from "react-data-export";
import { Button } from "react-bootstrap";

const ExcelFile = ReactExport.ExcelFile;
const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;

// const tableData=()=>{
//     row.map()
// }
const downloadButton = {
  backgroundColor: "#BED1FC",
  color: "#426488",
  borderColor: "#BED1FC",
};

const multiDataSet = (columnData, rowData) => [
  {
    columns: columnData.map(data => ({ title: data })),
    data: rowData.map(item =>
      columnData.map(ditem => ({ value: item[ditem] }))
    ),
  },
];

export default function DownloadSheet({ columns, row }) {
  console.log("COLUMNS&&&&&&888", columns);
  // console.log("download Rows",row);
  columns.map(data => <>{console.log("worst day ever", { data })}</>);
  return (
    <div>
      <ExcelFile
        element={
          <Button
            style={downloadButton}
          >
            Download
          </Button>
        }
      >
        <ExcelSheet dataSet={multiDataSet(columns, row)} name="Organization" />
      </ExcelFile>
    </div>
  );
}
