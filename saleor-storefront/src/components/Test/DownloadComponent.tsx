import React from "react";
import axios from "axios";
import { Button } from "react-bootstrap";
import GetAppIcon from "@material-ui/icons/GetApp";


export default function DownloadComponent({download}) {
  // console.log("REAL ITEM ", Object.keys(props.tableName)[0]);

  const downloadLinker = async (itemTable) => {
    try {
     await axios({
        url: "http://18.221.78.83:9000/plugins/baar/download/" + itemTable,
        headers: { "X-API-KEY": "u073hizT.txgbxT0VL793cyzaL9Kd5OBNtk5UV1f5" },
        method: "GET",
        responseType: "blob",
      }).then(response => {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", itemTable + ".xlsx"); //or any other extension
        document.body.appendChild(link);
        link.click();
      });
    } catch (err) {
      console.log(err);
    }
  };
  return (
    <div>
      {/* <Button
                    type="submit"
                    onClick={()=>downloadLinker(Object.keys(props.tableName)[0])}
                    variant="outline-success"
                  >
                    Download
                  </Button> */}
      <Button
        variant="outline-success"
        onClick={() => downloadLinker(download)}
      >
      Download
      </Button>
    </div>
  );
}
