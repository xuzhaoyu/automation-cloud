import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import Dialog from "@material-ui/core/Dialog";
import DialogContentText from "@material-ui/core/DialogContentText";
import {Spinner} from 'react-bootstrap';
// import { withStyles } from '@material-ui/core/styles';
// import MuiDialogContent from '@material-ui/core/DialogContent';
// import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
// import DialogTitle from "@material-ui/core/DialogTitle";
// import "./styles.css";

export default function Table({ open, setOpen }) {
  const [columns, setColumns] = useState([]);
  const [row, setRow] = useState([]);

  const loading = useSelector((state:any) => state.Array.loading);
  const tableArrayName = useSelector((state: any) => state.Array.tableName);
  const tableData = useSelector((state: any) => state.Array.tableData);

  // const data = React.useMemo(() => [...tableArrayName],[tableArrayName] )

  useEffect(() => {
    if (tableData && tableArrayName[0]) {
      setColumns(Object.keys(tableArrayName[0]));
      setRow(tableArrayName);
    }
  }, [tableData]);

  // console.log("TABLE_______",data);
  console.log("oops--", columns.splice(0,1));
  console.log("redux___", tableArrayName);

  // const rowTable = () => {
  //   return (
  //     columns &&
  //     columns.map((ditem, i) =>
  //       console.log("COL__________", Object.keys(ditem))
  //     )
  //   );
  // };
  // const DialogContent = withStyles((theme) => ({
  //   root: {
  //     padding: theme.spacing(2),
  //     width:"60ch",
  //   },
  // }))(MuiDialogContent);

  return (
    <div>
      <Dialog
        open={open}
        onClose={() => setOpen(false)}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogContent>
          <DialogContentText id="alert-dialog-description">

            {loading  ? (
              <>
                <Spinner animation="border" />
              </>
            ) : (
             <>
             <table >
                  <thead>
                    <tr>
                      {columns.map((item, i) => (
                        <th key={i}>{item.toUpperCase()}</th>
                      ))}
                    </tr>
                  </thead>
                  <tbody>
                    {row.map((item, i) => (
                      <tr key={i}>
                        {columns.map((ditem, i) => (
                          <td key={i}>{item[ditem]}</td>
                        ))}
                      </tr>
                    ))}
                  </tbody>
                </table>
             </>
            )}
          </DialogContentText>
        </DialogContent>
      </Dialog>
    </div>
  );
}
