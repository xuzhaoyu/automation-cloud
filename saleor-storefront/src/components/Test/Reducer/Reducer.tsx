import * as actionTypes from "../Action/index"


const initialState ={
   colVal:[],
   objects:[]
}
// interface BAAR_COMPONENT {
//     type:
// }

 const BaarComponentTablecolumns=(state=initialState,action)=>{
    
    switch (action.type) {
        case actionTypes.BAAR_COMPONENT_COLUMN_VALUES:
           return {
               ...state,
               colVal:action.payload.column,
           } 
           case actionTypes.BAAR_COMPONENT_TABLE_OBJECTS:
           return {
               ...state,
               objects:action.payload.objects,
           }            
    
        default:
           return state;
    }
}
export default BaarComponentTablecolumns;