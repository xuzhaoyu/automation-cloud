import * as actionTypes from "../Action/Actiontypes";

const initialState = {
  tableData: false,
  open:false,
  loader: false,
  loaderforTable: false,
  submit:null,
  tableArray: [],
  tableName: [],
  productName: null,
  data1: {},
  item1: {},
  editedData: {},
};

export const Tablereducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.DYNAMIC__TABLES:
      return (state = {
        ...state,
        tableArray: action.payload.dynTab,
        loaderforTable: false,
        productName: action.payload.productname,
      });

    case actionTypes.DYNAMIC__TABLE_NAMES:
      return (state = {
        ...state,
        tableData: true,
        loader: false,
        tableName: action.payload.dynName,
      });

    case actionTypes.CHANGE__TABLE__DATA:
      return (state = {
        ...state,
        tableName: [],
        tableData: false,
      });
      case actionTypes.OPEN_NAVBAR:
        return (state = {
          ...state,
          open:true
        });

        case actionTypes.CLOSE_NAVBAR:
          return (state = {
            ...state,
            open:false
          });

    case actionTypes.SETTABLENAME:
      return (state = {
        ...state,
        tableData: false,
        tableName: [],
      });

    case actionTypes.ADD_NEW_DATA:
      return (state = {
        ...state,
        data1: action.payload,
      });

    case actionTypes.EDIT_FORM_DATA:
      return (state = {
        ...state,
        item1: action.payload,
      });

    case actionTypes.EDITED_NEW_DATA:
      return (state = {
        ...state,
        editedData: action.payload,
      });

    case actionTypes.SET_LOADER_DATA:
      return (state = {
        ...state,
        loader: true,
      });

    case actionTypes.SET_LOADER_FOR_TABLE:
      return (state = {
        ...state,
        loaderforTable: true,
      });

    case actionTypes.SET_SUBMIT:
      return (state ={
        ...state,
        submit:action.payload
      })
    case actionTypes.SET_SUBMIT_FALSE:
      return ( state ={
        ...state,
        submit:false
      })
    default:
      return state;
  }
};
