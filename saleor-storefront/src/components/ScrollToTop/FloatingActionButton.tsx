import React from "react";
import { Fab, Zoom } from "@material-ui/core";
import "./index.css";

export default function FloatingActionButton() {


  return (
    <>
      <div className="help-desk-button">
        <Zoom in={true} timeout={{ enter: 500, exit: 500 }} unmountOnExit>
          <Fab variant="extended" className="fab-helpdesk" size="medium">
            HELP DESK
          </Fab>
        </Zoom>
      </div>
    </>
  );
}
