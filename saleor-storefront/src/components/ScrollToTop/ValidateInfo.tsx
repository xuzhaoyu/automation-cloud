
export default function ValidateInfo(values){
    let errors:any={}
 
if(!values.userName.trim()){
    errors.userName ="User Name is required";
  }
  if(!values.email){
      errors.email= "Email is required";
  }
  else if(!/\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/.test(values.email)){
      errors.email="Email is not valid"
  }
  if(!values.phone){
      errors.phone="Phone no is required";
  }
  else if(values.phone.length > 10){
      errors.phone="Not a valid Phone no"
  }
  if(!values.organisationName){
      errors.organisationName="Organisation Required"
  }
 if(!values.textArea){
     errors.textArea="Cannot be empty"
 }
  return errors;
}