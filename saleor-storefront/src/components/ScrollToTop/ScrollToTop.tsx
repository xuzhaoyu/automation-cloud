import React from "react";
import {useEffect,useState} from "react";
import FloatingActionButton from "./FloatingActionButton";
import { useSelector } from "react-redux";
import './index.css'
import PopupWindowHelpdesk from "./PopupWindowHelpdesk";
export const ScrollToTop =()=>{
    const submit = useSelector((state:any) => state.Array.submit)
    const [isVisible,setInvisible] = useState(false);
    const [popUp, setpopUp] = useState(false);
   
    const [isSubmitted, setIsSubmitted] = useState(false);
    
    // console.log("STORE SUBMIT",submit);
    
    const toggleVisibility =()=>{
        if(window.pageYOffset >300){
            setInvisible(true)
        }else{
            setInvisible(false)
        }
    }
    const ScrollToTop =()=>{
        window.scrollTo({
            top:0,
            behavior: 'smooth',
        });
    };
    
    useEffect(()=>{
        window.addEventListener('scroll',toggleVisibility);

        return()=>{
            window.removeEventListener('scroll',toggleVisibility)
        };
    },[]);

    const signup=()=>{
        setIsSubmitted(true);
    }

    return (
        <>
        {popUp ? <PopupWindowHelpdesk setpopUp={setpopUp}/> : ""}
        {/* {isSubmitted ? <PopupWindow setpopUp={setpopUp}/>:console.log("Submitted with Success7890780")
        } */}
        <div className={isVisible? "show scrollButton": "hide scrollButton"}>
           
            <button onClick={ScrollToTop}>
            <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                <path fillRule="evenodd" d="M14.707 12.707a1 1 0 01-1.414 0L10 9.414l-3.293 3.293a1 1 0 01-1.414-1.414l4-4a1 1 0 011.414 0l4 4a1 1 0 010 1.414z" clipRule="evenodd" />
            </svg>
            </button>
            <div onClick={()=>setpopUp(true)}>
           <FloatingActionButton />
           </div>
        </div>
       </>
    )
};
