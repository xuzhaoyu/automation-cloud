import React, { useState } from "react";
import CancelIcon from "@material-ui/icons/Cancel";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import validate from "./ValidateInfo";
import useForm from "./useForm";
import { useSelector, useDispatch } from "react-redux";
import { SetSubmit } from "../Test/Action/DynTable";
import "./index.css";

export default function PopupWindow({ setpopUp }) {
  const [close, setClose] = useState(true);
  const { handleChange, values, handleSubmit, errors, isSubmitting } = useForm(
    validate
  );
  const dispatch = useDispatch();
  // var event = document.getElementById("new-window");
  const textFieldStyle = {
    marginTop: "9px",
    alignItems: "center",
  };
  function closeHandler() {
    // dispatch(SetSubmit(false))
    setClose(false);
    setpopUp(false);
  }
  const submitFalse = useSelector((state: any) => state.Array.submit);
  console.log("window toggle", close);
  console.log("CLose submit button store", values);

   const isDisabled=()=>{
     let disable=true
     const {userName , email , organisationName , phone , textArea} = values
     
    if(userName&& email &&organisationName && phone && textArea){
      disable=false
    }
    else {
      disable=true
    }
    return disable;
   }
  return (
    <div id="new-window" className={close ? "window-helpdesk" : "notwindow-helpdesk"}>
      <div style={{ display: "flex", justifyContent: "flex-end" }}>
        <CancelIcon style={{ color: "orange" }} onClick={closeHandler} />
      </div>
      <div>
        <form onSubmit={handleSubmit} noValidate autoComplete="on">
          <div
            style={{ display: "flex", flexDirection: "column", width: "100%" }}
          >
            <h2 style={{ textAlign: "center", fontSize: "1.5rem" }}>
              Feel free to Contact Us !
            </h2>
            <TextField
              type="text"
              style={textFieldStyle}
              id="outlined-basic"
              label="User name"
              name="userName"
              value={values.userName}
              onChange={handleChange}
              error={Boolean(errors?.userName)}
              helperText={errors?.userName}
              autoFocus
              fullWidth
              required
            />

            {/* <TextField
              type="text"
              id="outlined-basic"
              style={textFieldStyle}
              label="Last name"
              name="lastName"
              error={errors?.lastName}
              helperText={errors?.lastName}
              value={values.lastName}
              onChange={handleChange}
              
              fullWidth
              required
            /> */}

            <TextField
              type="email"
              id="outlined-basic"
              style={textFieldStyle}
              label="email address"
              name="email"
              error={Boolean(errors?.email)}
              helperText={errors?.email}
              value={values.email}
              onChange={handleChange}
              fullWidth
              required
            />

            <TextField
              type="text"
              id="outlined-basic"
              style={textFieldStyle}
              label="Contact Number"
              name="phone"
              error={Boolean(errors?.phone)}
              helperText={errors?.phone}
              value={values.phone}
              onChange={handleChange}
              fullWidth
            />

            <TextField
              type="text"
              id="outlined-basic"
              style={textFieldStyle}
              label="Organisation Name"
              name="organisationName"
              error={Boolean(errors?.organisationName)}
              helperText={errors?.organisationName}
              value={values.organisationName}
              onChange={handleChange}
              fullWidth
            />
            <textarea
              id="w3review"
              name="w3review"
              style={textFieldStyle}
              rows="3"
              cols="30"
              name="textArea"
              value={values.textArea}
              onChange={handleChange}
              placeholder="How can we help..."
            ></textarea>
            {errors.textArea && <p style={{color:"red"}}>{errors.textArea}</p>}
            <Button
              className="window-button"
              type="submit"
              variant="contained"
              disabled={isDisabled()}
              fullWidth
            >
              Submit
            </Button>
          </div>
        </form>
      </div>
    </div>
  );
}
