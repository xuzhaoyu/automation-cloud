import React,{useState,useEffect} from "react";
import { useDispatch,useSelector } from "react-redux";
import {SetSubmit} from "../Test/Action/DynTable";

const useForm =(validate)=>{
    const [values, setValues] = useState({
        userName:"",
        lastName:"",
        email:"",
        phone:"",
        organisationName:"",
        textArea:"",
    })
    const dispatch=useDispatch()
    const [errors, setErrors] = useState({})
    const [isSubmitting, setIsSubmitting] = useState(false);
    const submit = useSelector((state:any) => state.Array.submit)

    console.log("----++++++____",submit);
    
    const handleChange =(e)=>{
        const {name,value}=e.target
        setValues({
            ...values,[name]:value
        })
        setErrors(validate(values))
    }
    console.log("Help desk values",values);
    
   useEffect(() => {
       dispatch(SetSubmit(isSubmitting))
   }, [dispatch])

    const handleSubmit= e=>{
        e.preventDefault();
        setErrors(validate(values));
        setIsSubmitting(true);
        console.log("------",isSubmitting);
        dispatch(SetSubmit(isSubmitting));
    }

    return {handleChange,values,handleSubmit,errors,isSubmitting}
}

export default useForm;