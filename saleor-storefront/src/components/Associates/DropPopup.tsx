import React, { useState } from 'react';
import Dialog from '@material-ui/core/Dialog';
// import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
// import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import 'date-fns';
import { useDispatch } from "react-redux";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { officialData } from '../Test/Action/DynTable';



export default function DropPopup({ open, setOpen }) {
    const dispatch = useDispatch();
    const [data, setData] = useState({
        // dateunique: '',
        officeid: ''
    })
    // const handleDateChange = (date: Date | null, event) => {
    //     setSelectedDate(date);
    // };
    const handleChange = (e) => {
        const fieldName = e.target.name;
        const fieldValue = e.target.value;
        setData({ ...data, [fieldName]: fieldValue })
    }
    console.log("submitted data tcws___", data);

    const handleSubmit = (e) => {
        e.preventDefault();
        dispatch(officialData(data))
        toast("Submitted Successfully");

    }
    return (
        <div>
            <ToastContainer />
            <Dialog
                open={open}
                onClose={() => setOpen(false)}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{"Information"}</DialogTitle>
                <DialogContent>

                    <form onSubmit={handleSubmit}>
                        <div style={{ display: 'flex', flexDirection: 'column' }}>
                            {/* <TextField
                                type="date"
                                name="dateunique"
                                onChange={handleChange}
                                required
                            /> */}
                            <TextField
                                type="text"
                                name="officeid"
                                id="standard-basic"
                                label="employee-id"
                                onChange={handleChange}
                                required />
                            <br></br>


                            <Button onClick={() => setOpen(false)} style={{ width: '100%' }} type="submit" variant="contained" color="primary">Submit</Button>

                        </div>
                    </form>

                </DialogContent>
            </Dialog>
        </div>
    )
}
