import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Row, Col, Container } from "react-bootstrap";
import DropPopup from './DropPopup';
import './styles.css';
import Navbar from './Navbar';
// import Grid from '@material-ui/core/Grid';


const useStyles = makeStyles((theme) => ({
    root: {
        marginTop: '24px',
    },
    // card: {
    //     marginBottom: "20px",
    //     background: '#016184',
    //     color: 'white'
    // },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 20,
        color: 'white',
        maxWidth:'auto',
    },
    subtitle:{
        color:'#577BC1',
        fontSize:16,
    },
    pos: {
        marginBottom: 12,
    },
    cardaction:{
        padding:10,
        borderTop:'1px solid white',
        display:'flex',
        justifyContent:'center',
        alignItems:'center'
    },
    cardcontent:{
        height:200,
    }
}));

export default function associates() {
    const classes = useStyles();
    const [open, setOpen] = useState(false)
    const bull = <span className={classes.bullet}>•</span>;
    const arrJson = [
        {
            name: "Last Months Salary Slip",
            value: "Use this option if you would like to receive your last months salary slip."
        },
        {
            name: "Need 1 month Salary Advance",
            value: "1 Month salary advance based on eligibility"
        },
        {
            name: "Leave Balance Statement",
            value: "Get Current Years Leave Statemen"
        },
        {
            name: "Leave Balance Statement-Previous Year",
            value: "Get Previous Year's Leave Balance Statement"
        },
        {
            name: "Last 3 months Salary Slip",
            value: "Get last 3 months salary slip"
        },
        {
            name: "Last 1 Year Salary Slips",
            value: "Get last 12 Months Salary slips"
        },
        {
            name: "Paid Days",
            value: "How many days salary was I paid for?"
        },
        {
            name: "Reimbursements recieved",
            value: "What reimbursements have I received in the last months salary ?"
        },
        {
            name: "Leave Entitlements",
            value: "What are leave entitlements?"
        },
        {
            name: "Leave Encashment",
            value: "How many leaves can I encash?"
        },
        {
            name: "Airfare Eligibility",
            value: "How much am I allowed to claim for Airfare during the year?"
        },
        {
            name: "Salary Disbursement",
            value: "When was my salary released?"
        },
    ]
    return (
        <div>
            <Container>
                <Row style={{ padding: "2rem", marginTop: '2rem' }}>
                    {arrJson.map((item, ind) => (
                        <Col lg={3} md={4} xs={12} sm={12} key={ind}>
                            <div className={classes.root}>
                                <Card className="card-style" >
                                    <CardContent className={classes.cardcontent}>
                                        <Typography className={classes.title} color="textSecondary" gutterBottom>
                                            {item.name}
                                        </Typography>
                                        <Typography className={classes.subtitle} variant="body2" component="p">
                                            {item.value}
                                        </Typography>
                                    </CardContent>
                                    <CardActions className={classes.cardaction}>
                                        
                                        <Button style={{color:'white'}} onClick={() => setOpen(true)} size="small">REQUEST</Button>
                                    </CardActions>
                                </Card>

                            </div>
                        </Col>
                    ))}
                </Row>

                <DropPopup open={open} setOpen={setOpen} />
            </Container>
        </div>

        // </div>
    )
}
