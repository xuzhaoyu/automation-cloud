import React from 'react';
import { useDispatch } from 'react-redux';
import { closeNavbar } from '../Test/Action/DynTable';
import { Link } from 'react-router-dom';

import './styles.css'



function Navbar() {
    const dispatch = useDispatch()
    return (
        
        <div className='navbar'>

            <div className="tasc">

                <div className="logo">

                <Link to="/"><img onClick={()=>dispatch(closeNavbar())}  src="https://tascoutsourcing.com/images/logo.svg" alt="tasclogo" /></Link>
                      


                </div>
                <div className="leftMenu">

                    <ul>

                        <li className='active' > <a href="#" />Associates</li>

                        <li> <a href="#" />Clients</li>

                        <li> <a href="#" />Robots</li>

                        <li> <a href="#" />Client Dashboard</li>

                        <li> <a href="#" />Associates Dashboard</li>

                    </ul>

                </div>



            </div>

            <div className="rightMenu">

                <ul>

                    <li>

                        <a href="#">Register</a>

                    </li>

                    <li>

                        <a href="#">Admin Tasks</a>

                    </li>

                    <input type="submit" value="Logout"  className='tasc-button'/>

                </ul>



            </div>



        </div >

    )

}



export default Navbar