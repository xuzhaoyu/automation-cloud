import "./scss/index.scss";

import * as React from "react";

import { SocialMediaIcon } from "..";
import { SOCIAL_MEDIA } from "../../core/config";
import Nav from "./Nav";
import BaarLogo from "../../images/BaarLogoNewW.png"
import ReactSVG from "react-svg";

const Footer: React.FC = () => (
  <div className="footer">
    <div className="footer__logo">
      <img src={BaarLogo} alt="logoBaar" />
    </div>
    <div className="footer_li">
      <Nav />
    </div>
    <div className="footer__social">
      <a target="_blank" href="https://www.facebook.com/alliedmediainc"><i className="fab fa-facebook"></i></a>
      <a target="_blank" href="https://www.linkedin.com/company/alliedmedia"><i className="fab fa-linkedin"></i></a>
      <a target="_blank" href="https://www.youtube.com/channel/UChxs-pq9wSK22Fv5ny1eWxw"><i className="fab fa-youtube"></i></a>

    </div>
  </div>
  // <div className="footer" id="footer">
  //   <div className="footer__favicons container">
  //     {SOCIAL_MEDIA.map(medium => (
  //       <SocialMediaIcon medium={medium} key={medium.ariaLabel} />
  //     ))}
  //   </div>
  //   <Nav />
  // </div>
);

export default Footer;
