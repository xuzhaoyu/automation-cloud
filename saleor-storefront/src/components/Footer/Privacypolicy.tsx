import React from 'react'
import "./scss/index.css"
import {Container} from "react-bootstrap"

export default function Privacypolicy() {
    return (
        <div>
            <Container>
           <h1 className="header-tag"><b>Introduction and overview of BAAR Cloud’s Approach to Data Protection and Privacy</b></h1>

           <p style={{padding:"4%",textAlign:"left"}}>
            References to “We”, “us” or “Allied Media” in this statement mean Allied Media Inc. or one of their affiliated entities. Our contact details for these entities and their respective office locations can be found <span style={{color:"blue"}}><a target="_blank" href="https://www.alliedmedia.com/contact">here.</a></span> References to “You” or “your” mean the corporation or individual person (as appropriate in the circumstances) who has or may in the future enter into a relationship with Allied Media as a customer, vendor, authorized channel partner, employee or otherwise uses the Allied Media website.
            At Allied Media, we pride ourselves on being an organization that has a privacy-minded culture consistent with legal requirements. We will ensure that your personal data is: (a) processed by Allied Media lawfully, fairly and transparently; (b) only collected for clear and legitimate purposes; (c) limited in scope and time to only the extent necessary for the purpose of that processing; (d) kept accurate and up-to-date; (e) secured against unauthorized or unlawful processing and against accidental loss, destruction or damage.
            This privacy policy describes how Allied Media will process personal data in connection with your use of BAAR Cloud’s products or services (“BAAR Cloud’s Services”), this website and other technical applications, tools or services, or a job application to work at Allied Media.
           </p>

           <h1 className="header-tag"><b>What Allied Media entity is responsible for processing your personal data?</b></h1>

           <p style={{padding:"2%",textAlign:"left"}}>
            The Allied Media entity that will be responsible for processing your personal data will depend on how you use BAAR Cloud’s Services and your geographical location but will most likely include BAAR Cloud’s headquarters in Canada and India as BAAR Cloud’s principal places of business.
            The relevant Allied Media entity will be referred to as “Allied Media” in this policy. A full list of BAAR Cloud’s entities and contact information can be found <span style={{color:"blue"}}><a target="_blank" href="https://www.alliedmedia.com/contact">here.</a></span>
            You can contact Allied Media at any time to request more information about the way we process personal data via privacy-request@alliedmedia.com. We will respond to your request in the timescales prescribed by the relevant local laws.
            </p>
            </Container>

            <div style={{backgroundColor:"blue",width:"100%",color:"white"}}>
            <h1 className="header-tag">What personal data does Allied Media process?</h1>
            </div>

            <Container>
            <p style={{paddingTop:"20px"}}>
            <strong>The personal data that Allied Media processes will vary based on your relationship with Allied Media, but may include:</strong>
            </p>

            <ul style={{listStyleType:"disc",padding:"2%"}}>
                {/* para */}
               <p className="para">
                Where you are a third party with whom Allied Media has, has had or may have a business relationship, certain business contact information (e.g. business email addresses, business telephone numbers and names) in relation to performance of any contract with you or to pursue our legitimate interests
               </p><br></br>
                <li>
                where you are an existing customer of BAAR Cloud’s Services, so that we can provide you and your company with the Allied Media Services (such as maintenance and
                support services) that you have purchased and meet our contractual obligations and exercise any contractual rights (for example, invoicing you for payment).
                </li>
                <li>
                where you are an authorized channel partner, to manage your account for the Partner Portal and your use of the Partner Portal, including responding to questions you have raised
                </li>
                <li>
                where you are any other third party with whom Allied Media currently has, in the past had, or may in the future have a contractual relationship (for example a supplier of goods or services to Allied Media) to provide you with, receive from you or jointly pursue with you any relevant goods or services where you have expressed an interest in BAAR Cloud’s Services, attended a Allied Media hosted or sponsored conference or event, or downloaded any know-how from our website, so that we can explore potential solutions for you and your business
                </li>
                <li>
                to notify you about developments, improvements or issues relating to BAAR Cloud’s Services or related events, or otherwise where we process business contact information about you for our legitimate interests in maintaining records regarding how our customers use our products/services
                </li>
                <li>
                to operate our business, for example transmitting your personal data within the Allied Media group for internal administrative purposes, such as auditing and accounting
                </li>
                <li>
                to undertake “know-your-client” and anti-fraud checks to help prevent any illegal activity, comply with applicable laws and requests from regulators and other enforcement bodies, or otherwise administer and protect our business and this website
                To opt-out of receiving communications relating to marketing, events or promotions from Allied Media, you can contact us at any time via privacy-request@alliedmedia.com. We will record the fact that you have opted-out of receiving marketing or promotional communications from us, unless you ask us to erase all personal data that Allied Media holds on you. Please note that if you are an existing customer then we may need to retain business contact information in order to provide you with BAAR Cloud’s Services. Where you have entered into a written agreement with Allied Media which describes in more detail how Allied Media will process, store, handle or retain any such information, then that agreement will prevail. Please note that revoking your consent to our use of your business contact information could prevent us from providing you with certain Allied Media Services.
                </li><br></br>
                    {/* para */}
                <p className="para">
                Where you use our website or other products, certain identity information (e.g. names, usernames or similar identifiers) and technical information (e.g. IP address, login credentials, browser type and version, location data and relevant plug-ins, operating systems and platforms employed by your device(s)) to administer our website or pursue our legitimate interests
                </p><br></br>

                <li>
                To provide you with relevant website content and advertisements and measure the effectiveness of such content in your use of the website and resulting products/services
                </li>
                <li>
                Through the use of data analytics to improve such content and advertisements, your use of our products/services and other aspects of our business and your overall customer experience
                </li><br></br>
                    {/* para */}
                <p className="para">
                Where you are applying for a role with Allied Media Personal details (including your home address, telephone number and work history) to administer such application, perform certain legal obligations and to pursue our legitimate interests   
                </p><br></br>

                <li>
                In order to process your application for a specific job vacancy and keep you updated in relation to any changes to such vacancies
                </li>

                <li>
                to notify you about upcoming jobs or other information concerning your application or job search. 
                </li>
            </ul>

                <p>
                From time to time we may use publicly accessible sources – such as corporate websites and social networking platforms – to obtain business contact information (as defined above), or purchase databases containing business contact information from third parties, where we reasonably believe that such companies or persons may be interested in hearing more about BAAR Cloud’s Services. If Allied Media contacts you as a result of this activity and you are not interested in BAAR Cloud’s Services then we will record your preference on our internal systems to ensure that we minimize the possibility of contacting you again in the future.
                </p>

                <h1 className="header-tag">Will Allied Media share your personal data with third parties</h1>
                <p>
                Allied Media won’t sell or provide any third parties with your personal data without first obtaining your consent.
                The only exception to this general rule is where we use a third party to host certain information for us which may include your personal data, are required to share your personal data with third parties so that we can comply with certain legal obligations, or to make sure we comply with our own audit and security requirements, internal policies and procedures or other legal and contractual obligations.
                Allied Media may also share anonymized or aggregated data containing your personal data to third parties in order to improve our services and internal practices, but in any case, where a third party is involved that third party will only have access to anonymized data and will not be able to identify you as an individual.
                If you are based in the European Economic Area (“EEA”), your information might be accessed by other Allied Media companies within our corporate group which are based outside of the EEA. All transfers of personal data within the Allied Media group are subject to adequate safeguards, normally in the form of the European Commission’s Standard Contractual Clauses (or “Model Clauses”).
                </p>

                <h1 className="header-tag">How will Allied Media store your personal data?</h1>
                <p>
                If you are a Allied Media customer, Allied Media will store your personal data for the period that you continue to receive Allied Media Services and for an appropriate period of time thereafter, which enables Allied Media to comply with applicable laws (for example, for a period of 6 years in respect of any financial or transactional data where you have a business relationship with us for tax and audit purposes) as well as its internal data retention policy, a copy of which is available from us on request (for example, for the purposes of complying with any audit or accounting processes, or complying with the terms of any legal action).
                If you are not yet an Allied Media customer, then Allied Media will store your personal data for the duration of any pre-sales activities, or to record the fact that you are not interested in purchasing any Allied Media Services (to avoid you receiving unwanted communications from Allied Media).
                If you are applying for a role with Allied Media, we will store your personal data for no more than 24 months. At the end of that period we will remove all of your personal data from our systems. By submitting your application and CV, you consent to Allied Media storing your data in the ways and for the period of time described in this statement. For more detail on how we will process your personal data in connection with an application to work at Allied Media, please visit our <span style={{color:"blue"}}><a href="https://www.alliedmedia.com/careers">careers webpage.</a></span>  You may contact us at any time via <span><strong>privacy-request@alliedmedia.com</strong></span> to request access to your personal data, to correct any information which Allied Media holds on you that contains an error or to request that we erase some or all of the personal data that we hold relating to you, or to request a copy of our retention policy. If you are an existing Allied Media customer, then revoking your consent to our use of business contact information could prevent us from providing you with certain Allied Media Services.
                We will maintain administrative, physical and technical safeguards designed to protect the security, confidentiality and integrity of your personal data processed by us as part of your use of our products/services, this website and any other aspects of our business as described in this policy; and will not materially decrease the overall security of such items.
                </p>

                <h1 className="header-tag">Where you are a partner using BAAR Cloud’s Partner Portal</h1>
                <p>
                Where you are one of BAAR Cloud’s authorized channel partners, we will process information (including certain personal data) that you upload to the <span style={{color:"blue"}}><a href="https://www.alliedmedia.com/partners">Partner Portal.</a></span>  For example, when you register on the Partner Portal, we will collect your account name, contact details and job function.
                If you submit a Deal Registration Form via the Partner Portal, we will also collect the following information: corporate name of end customer and contact details for your point of contact within the end customer (including name, job title and address). Where you provide Allied Media with such personal data, you agree that you have first sought all necessary consents and authorizations from relevant individuals to enable Allied Media to comply with all applicable laws.
                Further details regarding information that is not personal data but may be required in order to effectively use the Partner Portal and complete a Deal Registration Form can be located on the Partner Portal.
                Any marketing consents opt-ins/opt-outs or other preference details provided to us in connection with another website or service operated by us (such as the Allied Media community or our transactional websites) will be recorded and administered separately from any preferences or consents provided in connection with the Partner Portal. You have the option to change your preferences registered in connection with any of our sites or services at any time.
                If you are an authorized channel partner and no longer want us to contact you related to marketing events or information, please contact us at <span><strong>channel@alliedmedia.com.</strong></span>
                </p>

                <h1 className="header-tag">How to contact us regarding or complain about BAAR Cloud’s processing of personal data</h1>

                <p>
                Depending on your location and the circumstances of our processing, you will have certain rights relating to your personal data under data protection laws. These include the right to: (i) request access to, correction of or deletion of your personal data; (ii) object to or otherwise restrict BAAR Cloud’s processing of your personal data; (iii) withdraw consent from our processing of your personal data.

                If you wish to exercise any of the rights set out above, or would like to complain about any aspect of BAAR Cloud’s processing of your personal data, then please contact us via <span><strong>privacy-request@alliedmedia.com.</strong></span> 
                While we would always appreciate the chance to deal with your concerns before you approach an external regulator, you can also contact a data protection supervisory authority in any of the countries in which Allied Media is established and you are based, such as the Information Commissioner’s Office in Canada.
                </p>

                <h1 className="header-tag">How Allied Media uses Cookies in relation to your use of this website</h1>

                <p>
                BAAR Cloud’s website will transfer small data files to your device’s hard drive (“Cookies”) when you use our website. Cookies allow us to identify your computer so that Allied Media may customize its services to better meet your needs. Most browsers automatically accept Cookies by default, but you can refuse Cookies by adjusting the preferences in your browser. If you refuse to accept Cookies some features on the Allied Media website may not work properly or may be unavailable to you. Accepting Cookies does not give Allied Media access to your computer.

                If you are an authorized channel partner, each time you use the Partner Portal we may also collect information including domain name/IP address, referring URL, browser and platform, time of visit, pages visited and any searches performed. This information is not itself personal data but may become personal data when used in conjunction with other information that you upload to the Partner Portal. We may use this information to help us improve the performance of the Partner Portal and to assess the suitability of, or requirements for, certain services on the Partner Portal.

                Allied Media uses session-based Cookies on this website to improve your interaction with our website. These Cookies only exist for your visit to our website on that occasion and are removed automatically from your computer or device once you close your browser or turn-off your device.

                BAAR Cloud’s website may connect you to content or services hosted by third parties with whom Allied Media either partners, or where we identify content which we think may be relevant or of interest to you. The use of Cookies by such third party websites is not covered by this privacy statement. Allied Media does not have access to or control over Cookies or content on these websites and they are used entirely at your own risk.
                </p>

            </Container>
        </div>
    )
}
