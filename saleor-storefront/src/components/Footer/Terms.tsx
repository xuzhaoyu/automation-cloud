
 const files = [
    {
      id: "1",
      term: "7.1. Access to Hosted Services (where applicable)",
      para: "Subject to the terms and conditions of these End Customer T&Cs, Allied Media grants you a non-exclusive right to access and use the Hosted Services during the applicable Subscription Term (as defined below) in accordance with these End Customer T&Cs, your applicable Scope of Use, and the Documentation. If Allied Media offers client software (e.g., a desktop or mobile application) for any Hosted Service, you may use such software solely with the Hosted Service, subject to the terms and conditions of these End Customer T&Cs. You acknowledge that our Hosted Services are on-line, subscription-based products and that we may make changes to the Hosted Services from time to time."
    },
    {
      id: "2",
      term: "7.2. Subscription Terms and Renewals",
      para: "Hosted Services are provided on a subscription basis for a set term specified in your Order (“Term”). Except as otherwise specified in your Order, all subscriptions will automatically renew for periods equal to your initial Term (and you will be charged at the then-current rates) unless you cancel your subscription. If you cancel, your subscription will terminate at the end of then-current billing cycle, but you will not be entitled to any credits or refunds for amounts accrued or paid prior to such termination"
    },
    {
      id: "3",
      term: "7.3. Credentials.",
      para: "You must ensure that all Authorized Users keep their user IDs and passwords for the Hosted Services strictly confidential and not share such information with any unauthorized person. User IDs are granted to individual, named persons and may not be shared. You are responsible for all actions taken using your accounts and passwords, and you agree to immediately notify Allied Media of any unauthorized use of which you become aware"
    },
    {
      id: "4",
      term: "7.4. Your Data.",
      para: "“Your Data” means any data, content, code, video, images or other materials of any type that you upload, submit or otherwise transmit to or through Hosted Services. You will retain all right, title and interest in and to Your Data in the form provided to Allied Media. Subject to the terms of these End Customer T&Cs, you hereby grant to Allied Media a non-exclusive, worldwide, royalty-free right to (a) collect, use, copy, store, transmit, modify and create derivative works of Your Data, in each case solely to the extent necessary to provide the applicable Hosted Service to you and (b) for Hosted Services that enable you to share Your Data or interact with other people, to distribute and publicly perform and display Your Data as you (or your Authorized Users) direct or enable through the Hosted Service. Allied Media may also access your account or instance in order to respond to your support requests."
    },
    {
      id: "5",
      term: "7.5. Security",
      para: "Allied Media implements security procedures to help protect Your Data from security attacks. However, you understand that use of the Hosted Services necessarily involves transmission of Your Data over networks that are not owned, operated or controlled by us, and we are not responsible for any of Your Data lost, altered, intercepted or stored across such networks. We cannot guarantee that our security procedures will be error-free, that transmissions of Your Data will always be secure or that unauthorized third parties will never be able to defeat our security measures or those of our third-party service providers."
    },
    {
      id: "6",
      term: "7.6. Storage Limits",
      para: "There may be storage limits associated with a particular Hosted Service. These limits are described in the services descriptions on our websites or in the Documentation for the particular Hosted Service. Allied Media reserves the right to charge for additional storage or overage fees at the rates specified on our website. We may impose new, or may modify existing, storage limits for the Hosted Services at any time in our discretion, with or without notice to you."
    },
    {
      id: "7",
      term: "7.7. Responsibility for Your Data.",
      para: "7.7.1. Generally. You must ensure that your use of Hosted Services and all Your Data is at all times compliant with our Terms and Conditions and all applicable local, state, federal, and international laws and regulations (“Laws”). You represent and warrant that: (i) you have obtained all necessary rights, releases and permissions to provide all Your Data to Allied Media and to grant the rights granted to Allied Media in these End Customer T&Cs and (ii) Your Data and its transfer to and use by Allied Media as authorized by you under these End Customer T&Cs do not violate any Laws (including without limitation those relating to export control and electronic communications) or rights of any third party, including without limitation any intellectual property rights, rights of privacy, or rights of publicity, and any use, collection and disclosure authorized herein is not inconsistent with the terms of any applicable privacy policies. Other than its security obligations under Section 7.5 (Security), Allied Media assumes no responsibility or liability for Your Data, and you shall be solely responsible for Your Data and the consequences of using, disclosing, storing, or transmitting it. 7.7.2. Sensitive Data. You will not submit to the Hosted Services (or use the Hosted Services to collect): (i) any personally identifiable information, except as necessary for the establishment of your Allied Media account; (ii) any patient, medical or other protected health information regulated by HIPAA or any similar federal or state laws, rules or regulations; or (iii) any other information subject to regulation or protection under specific laws such as the Gramm-Leach-Bliley Act (or related rules or regulations) ((i) through (iii), collectively, “Sensitive Data”). You also acknowledge that Allied Media is not acting as your Business Associate or subcontractor (as such terms are defined and used in HIPAA) and that the Hosted Services are not HIPAA compliant. “HIPAA” means the Health Insurance Portability and Accountability Act, as amended and supplemented. Notwithstanding any other provision to the contrary, Allied Media has no liability under these End Customer T&Cs for Sensitive Data. 7.7.3. Indemnity for Your Data. You will defend, indemnify and hold harmless Allied Media from and against any loss, cost, liability or damage, including attorneys’ fees, for which Allied Media becomes liable arising from or relating to any claim relating to Your Data, including but not limited to any claim brought by a third party alleging that Your Data, or your use of the Hosted Services in breach of these End Customer T&Cs, infringes or misappropriates the intellectual property rights of a third party or violates applicable law. This indemnification obligation is subject to your receiving (i) prompt written notice of such claim (but in any event notice in sufficient time for you to respond without prejudice); (ii) the exclusive right to control and direct the investigation, defense, or settlement of such claim; and (iii) all reasonable necessary cooperation of Allied Media at your expense."
       
    },
    {
      id: "8",
      term: "7.8. Removals and Suspension.",
      para: "Allied Media has no obligation to monitor any content uploaded to the Hosted Services. Nonetheless, if we deem such action necessary based on your violation of these End Customer T&Cs or in response to takedown requests that we receive following our guidelines for Reporting Copyright and Trademark Violations, we may (1) remove Your Data from the Hosted Services or (2) suspend your access to the Hosted Services. We will generally alert you when we take such action and give you a reasonable opportunity to cure your breach, but if we determine that your actions endanger the operation of the Hosted Service or other users, we may suspend your access immediately without notice. You will continue to be charged for the Hosted Service during any suspension period. We have no liability to you for removing or deleting Your Data from or suspending your access to any Hosted Services as described in this section."
    },
    {
      id: "9",
      term: "7.9. Deletion at End of Subscription Term",
      para: "We may remove or delete Your Data within a reasonable period of time after the termination of your Subscription Term."
    },
    {
      id: "10",
      term: "7.10. Service-Specific Terms.",
      para: "Hosted Services are provided on a subscription basis for a set term specified in your Order (“Term”). Except as otherwise specified in your Order, all subscriptions will automatically renew for periods equal to your initial Term (and you will be charged at the then-current rates) unless you cancel your subscription. If you cancel, your subscription will terminate at the end of then-current billing cycle, but you will not be entitled to any credits or refunds for amounts accrued or paid prior to such termination"
    }
  ]

export default files;