import React from "react";
import "./scss/index.css";
import { Container } from "react-bootstrap";
import terms from "./Terms";
// import { map } from "rxjs/operators";
// import 'rxjs/add/operator/map'

export default function Termscondition() {
  return (
    <div>
      <section>
        <h1 className="header-tag">
          <u>
            End Customer Terms and Conditions for the use of BAAR Cloud or any
            other Allied Media products.
          </u>
        </h1>
        <Container>
          <div>
            {/* <h1 className="vertical-para">End Customer Terms and Conditions</h1> */}
            <h3>Allied Media End Customer Terms & Conditions</h3>
            <h1>BAAR Cloud End Customer Terms & Conditions</h1>

            <p>
              These Allied Media End Customer Terms & Conditions (the “End
              Customer T&Cs”) are between you and Allied Media Incorporated,
              located at 2 Robert Speck Parkway Suite 750, Mississauga, Ontario,
              L4Z 1H8, Canada, (if you are contracting in North America) or
              Allied Media Innotech Pvt. Ltd., located at Tower 2A, 6TH Floor,
              Ecospace Business Park Premises, Action Area II Rajarhat Kolkata
              700156, India (if you are not contracting in North America)
              (collectively, “Allied Media”). If you are agreeing to these End
              Customer T&Cs not as an individual but on behalf of your company,
              then “Customer” or “you” means your company, and you are binding
              your company to these End Customer T&Cs. Allied Media may modify
              these End Customer T&Cs from time to time, subject to the terms in
              Section 26 (Changes to these End Customer T&Cs) below.
            </p>
            <br></br>
            <p>
              <strong>
                By using or accessing BAAR Cloud or any other Allied Media
                products, you indicate your assent to be bound by these End
                Customer T&Cs.
              </strong>
            </p>
          </div>

          <section>
            <ol type="1">
              <li>1. Scope of the End Customer T&Cs.</li>
              <p>
                These End Customer T&Cs govern your initial purchase as well as
                any future purchases made by you that reference these End
                Customer T&Cs. These End Customer T&Cs includes our{" "}
                <span>
                  <a
                    target="_blank"
                    href="https://www.alliedmedia.com/privacy-policy"
                  >
                    Privacy Policy,
                  </a>
                </span>{" "}
                ,{" "}
                <span>
                  <a
                    target="_blank"
                    href="https://www.alliedmedia.com/terms-of-the-website"
                  >
                    our Terms & Conditions for Allied Media Websites,
                  </a>
                </span>{" "}
                any Orders, and any other referenced policies and attachments.
                These End Customer T&Cs applies to Allied Media enhancements,
                add-ons or plugins that you purchase or receive from Allied
                Media
              </p>
              <li>2. Types of Allied Media Products</li>
              <p>
                These End Customer T&Cs govern (a) Allied Media’s downloadable
                software products (currently designated as “Server” or “Data
                Center” deployments) (“Software”), (b) Allied Media’s hosted or
                cloud-based solutions (currently designated as “Cloud”
                deployments) (“Hosted Services”), and (c) any related support or
                maintenance services provided by Allied Media. Software and
                Hosted Services, together with related Documentation, are
                referred to as “Products”. The Products and their permitted use
                are further described in Allied Media’s standard documentation
                (“Documentation”). Section 6 (Software Terms) applies
                specifically to Software, and Section 7 (Hosted Services Terms)
                applies specifically to Hosted Services, but unless otherwise
                specified, other provisions of these End Customer T&Cs apply to
                all Products
              </p>
              <li>3. Account Registration.</li>
              <p>
                You may need to register for an Allied Media account in order to
                place orders or access or receive any Products. If you do so,
                any registration information that you provide to us must be
                accurate, current and complete. You must also update your
                information so that we may send notices, statements and other
                information to you by email or through your account. You are
                responsible for all actions taken through your accounts.
              </p>
              <li>4. Orders.</li>
              <p className="sub-para">4.1. Directly with Allied Media.</p>
              <p>
                Allied Media’s Product ordering documentation or purchase flow
                (“Order”) will specify your authorized scope of use for the
                Products, which may include: (a) number and type of Authorized
                Users (as defined below), (b) storage or capacity (for Hosted
                Services), (c) numbers of licenses, copies or instances (for
                Software), or (d) other restrictions or billable units (as
                applicable, the “Scope of Use”). The term “Order” also includes
                any applicable Product or Support and Maintenance renewal, or
                purchases you make to increase or upgrade your Scope of Use.
              </p>
              <p className="sub-para">4.2. Reseller Orders.</p>
              <p>
                These End Customer T&Cs apply whether you purchase our Products
                directly from Allied Media or through Allied Media-authorized
                resellers (each, a “Reseller”). If you purchase through a
                Reseller, your Scope of Use shall be as stated in the Order
                placed by Reseller for you, and Reseller is responsible for the
                accuracy of any such Order. Resellers are not authorized to make
                any promises or commitments on Allied Media’s behalf, and we are
                not bound by any obligations to you other than what we specify
                in these End Customer T&Cs
              </p>

              <li>5. Authorized Users.</li>
              <p>
                Authorized Users may be your or your Affiliates’ employees,
                representatives, consultants, contractors, agents, or other
                third parties who are acting for your benefit or on your behalf.
                You may also permit your customers to have limited access to
                certain Products as Authorized Users. For certain Products,
                per-user pricing may apply. In the event that per-user pricing
                applies, it will be set forth on the applicable Order form for
                such Product. You are responsible for compliance with these End
                Customer T&Cs by all Authorized Users. All use of Products by
                you and your Authorized Users must be within the Scope of Use
                and solely for the benefit of you or your Affiliates.
                “Affiliate” means an entity which, directly or indirectly, owns
                or controls, is owned or is controlled by or is under common
                ownership or control with a party, where “control” means the
                power to direct the management or affairs of an entity, and
                “ownership” means the beneficial ownership of 50% (or, if the
                applicable jurisdiction does not allow majority ownership, the
                maximum amount permitted under such law) or more of the voting
                equity securities or other equivalent voting interests of the
                entity.
              </p>
              <li>6. Software Terms.</li>

              <p className="sub-para">6.1. Your License Rights.</p>
              <p>
                Subject to the terms and conditions of these End Customer T&Cs,
                Allied Media grants you a non-exclusive, non-sublicensable, and
                non-transferable license to install and use the Software during
                the applicable License Term in accordance with these End
                Customer T&Cs, your applicable Scope of Use, and the
                Documentation. The term of each Software license (“License
                Term”) will be specified in your Order. Your License Term will
                end upon any termination of these End Customer T&Cs, even if it
                is identified as “perpetual” or if no expiration date is
                specified in your Order. The Software requires a license key in
                order to operate, which will be delivered as described in
                Section 10.2 (Delivery).
              </p>

              <p className="sub-para">6.2. Number of Instances.</p>
              <p>
                Unless otherwise specified in your Order, for each Software
                license that you purchase, you may install one production
                instance of the Software on systems owned or operated by you (or
                your third party service providers so long as you remain
                responsible for their compliance with the terms and conditions
                of these End Customer T&Cs).
              </p>

              <p className="sub-para">6.3. Your Modifications.</p>
              <p>
                Subject to the terms and conditions of these End Customer T&Cs:
                (1) for any elements of the Software provided by Allied Media in
                source code form, and to the extent permitted in the
                Documentation, you may modify such source code solely for
                purposes of developing bug fixes, customizations and additional
                features for the Software and (2) you may also modify the
                Documentation to reflect your permitted modifications of the
                Software source code or the particular use of the Products
                within your organization. Any modified source code or
                Documentation constitutes “Your Modifications”. You may use Your
                Modifications solely with respect to your own instances in
                support of your permitted use of the Software, but you may not
                distribute the code to Your Modifications to any third party.
                Notwithstanding anything in these End Customer T&Cs to the
                contrary, Allied Media has no support, warranty, indemnification
                or other obligation or liability with respect to Your
                Modifications or their combination, interaction or use with our
                Products. You shall indemnify, defend and hold us harmless from
                and against any and all claims, costs, damages, losses,
                liabilities and expenses (including reasonable attorneys’ fees
                and costs) arising out of or in connection with any claim
                brought against us by a third party relating to Your
                Modifications (including but not limited to any representations
                or warranties you make about Your Modifications or the Software)
                or your breach of this Section 6.3. This indemnification
                obligation is subject to your receiving (i) prompt written
                notice of such claim (but in any event notice in sufficient time
                for you to respond without prejudice); (ii) the exclusive right
                to control and direct the investigation, defense, or settlement
                of such claim; and (iii) all reasonably necessary cooperation of
                Allied Media at your expense.
              </p>
              <p className="sub-para">6.4. Third Party Code.</p>
              <p>
                6.4.1. The Products contain code and libraries that we license
                from third parties. Some of these licenses require us to flow
                certain terms down to you. 6.4.2. Open Source Software in the
                Products. The Products include components subject to the terms
                and conditions of “open source” software licenses. To the extent
                applicable, we will identify open source software included in a
                Product in or through the Product itself. Some of these licenses
                require us to provide the open source software to you on the
                terms of the open source license instead of the terms of the End
                Customer T&Cs. In that case, the terms of the open source
                license will apply, and you will have the rights granted in such
                licenses to the open source software itself, such as access to
                source code, right to make modifications, and right to reverse
                engineer. Notwithstanding the foregoing, if you are using the
                Products in the form provided to you, in accordance with your
                permitted Scope of Use, with no distribution of software to
                third parties, then none of these open source licenses impose
                any obligations on you beyond what is stated in the End Customer
                T&Cs. 6.4.3. Combining the Products with Open Source Software. A
                requirement of some open source licenses, sometimes known as
                “copyleft licenses,” is that any modifications to the open
                source software, or combinations of the open source software
                with other software (such as by linking), must be made available
                in source code form under the terms of the copyleft license.
                Examples of copyleft licenses include the GPL or LGPL, Affero,
                CPL, CDDL, Eclipse, or Mozilla licenses. To the extent you are
                separately authorized by Allied Media to combine and distribute
                Products with any other code, you must make sure that your use
                does not: (i) impose, or give the appearance of imposing, any
                condition or obligation on us with respect to our Products
                (including, without limitation, any obligation to distribute our
                Products under an open source license); or (ii) grant, or appear
                to grant, to any third party any rights to or immunities under
                our intellectual property or proprietary rights in our Products.
                To be clear, you may not combine or otherwise modify our
                Products unless we expressly give you the right to do so under
                these End Customer T&Cs. 6.4.4. Commercial Third-Party Code in
                the Products. 6.4.4.1. The Products also include components that
                we license commercially from third parties (“Commercial
                Components”). For the avoidance of doubt, all of the
                restrictions for the Products in the End Customer T&Cs also
                apply to Commercial Components. Commercial Components are also
                subject to some additional requirements as set forth below.
                6.4.4.2. You may use Commercial Components only in conjunction
                with, as part of, and through the Products as provided by Allied
                Media. You may not install, access, configure or use any
                Commercial Components (including any APIs, tools, databases or
                other aspects any Commercial Components) separately or
                independently of the rest of the Product, whether for
                production, technical support or any other purposes, or
                otherwise attempt to gain direct access to any portions of the
                Commercial Components, or permit anyone else (including your
                customers) to do any of these things. 6.4.4.3. Some Commercial
                Components may include source code that is provided as part of
                its standard shipment. Commercial Component source code will be
                governed by the terms for Commercial Components in this
                supplement and not the provisions in Section 6.3 (“Your
                Modifications”) of these End Customer T&Cs. Accordingly,
                notwithstanding any other terms of these End Customer T&Cs, you
                may not modify any Commercial Components. You will be
                financially responsible to the applicable third-party licensor
                (“Commercial Component Licensor”) for all damages and losses
                resulting from your or your Authorized User’s breach of this
                Section. You may not “benchmark” or otherwise analyze
                performance information for individual Commercial Component
                elements. 6.4.4.4. You understand that the applicable Commercial
                Component Licensor retains all ownership and intellectual
                property rights to the Commercial Component. Commercial
                Component Licensors (and any other third-party licensors of any
                components of the Products) are intended third party
                beneficiaries of the End Customer T&Cs with respect to the items
                they license and may enforce the End Customer T&Cs directly
                against you. However, to be clear, Commercial Component
                Licensors do not assume any of Allied Media’s obligations under
                the End Customer T&Cs.
              </p>

              <li>7. Hosted Services Terms.</li>

              {terms.map(item => (
                <ul key={item.id}>
                  <p className="sub-para">{item.term}</p>
                  <p>{item.para}</p>
                </ul>
              ))}

              <li>8. Support and Maintenance.</li>
              <p>
                Allied Media will provide the support and maintenance services
                for the Products described in the Allied Media support and
                maintenance applicable to your Order (“Support and Maintenance”)
                during the period for which you have paid the applicable fee.
                Support and Maintenance is subject to the terms of the Allied
                Media Support Policy and will be provided at the support level
                and during the support term specified in your Order. If you have
                purchased the Products through an Allied Media Partner and not
                directly from Allied Media, the Support and Maintenance will be
                performed in accordance with Allied Media’s agreement with our
                Allied Media Partner. The Allied Media Support Policy may be
                modified by Allied Media from time to time to reflect process
                improvements or changing practices.
              </p>
              <li>9. Training Services.</li>
              <p>
                We will provide training services purchased in an Order in
                accordance with the descriptions and conditions for those
                services set forth in the Order and the accompanying service
                descriptions or datasheets (“Ancillary Services”). Allied Media
                shall retain all right, title and interest in and to any
                materials, deliverables, modifications, derivative works or
                developments related to any training services we provide
                (“Training Materials”). Any Training Materials provided to you
                may be used only in connection with the Products subject to the
                same use restrictions for the Products. If applicable, you will
                reimburse Allied Media for reasonable travel and lodging
                expenses as incurred.
              </p>

              <li>10. Returns and Financial Terms</li>
              <p className="sub-para">10.1. Delivery</p>
              <p>
                We will deliver the applicable Software or login instructions
                (in the case of Hosted Services) to the email addresses
                specified in your Order when we have received payment of the
                applicable fees. All deliveries under these End Customer T&Cs
                will be electronic. For the avoidance of doubt, you are
                responsible for installation of any Software, and you
                acknowledge that Allied Media has no further delivery obligation
                with respect to the Software.
              </p>
              <p className="sub-para">10.2. Payment</p>
              <p>
                You agree to pay all fees in accordance with each Order. Unless
                otherwise specified in your Order, you will pay all amounts in
                U.S. dollars (unless otherwise agreed) at the time you place
                your Order. Other than as expressly set forth in Section 20 (IP
                Indemnification by Allied Media), all amounts are
                non-refundable, non-cancelable and non-creditable. In making
                payments, you acknowledge that you are not relying on future
                availability of any Products beyond the current License Term or
                Subscription Term or any Product upgrades or feature
                enhancements. If you purchase any Products through a Reseller,
                you owe payment to the Reseller as agreed between you and the
                Reseller, but you acknowledge that we may terminate your rights
                to use Products if we do not receive our corresponding payment
                from the Reseller
              </p>
              <p className="sub-para">10.3. Taxes</p>
              <p>
                Your payments under these End Customer T&Cs exclude any taxes or
                duties payable in respect of the Products in the jurisdiction
                where the payment is either made or received. To the extent that
                any such taxes or duties are payable by Allied Media, you must
                pay to Allied Media the amount of such taxes or duties in
                addition to any fees owed under these End Customer T&Cs.
                Notwithstanding the foregoing, you may have obtained an
                exemption from relevant taxes or duties as of the time such
                taxes or duties are levied or assessed. In that case, you will
                have the right to provide to Allied Media any such exemption
                information, and Allied Media will use reasonable efforts to
                provide such invoicing documents as may enable you to obtain a
                refund or credit for the amount so paid from any relevant
                revenue authority if such a refund or credit is available.
              </p>

              <li>11. No-Charge Products.</li>
              <p>
                We may offer certain Products to you at no charge, including
                free accounts, trial use, demonstration accounts, and access to
                Beta Versions as defined below (“No-Charge Products”). Your use
                of No-Charge Products is subject to any additional terms that we
                specify and is only permitted for the period designated by us.
                You may not use No-Charge Products for competitive analysis or
                similar purposes. We may terminate your right to use No-Charge
                Products at any time and for any reason in our sole discretion,
                without liability to you. You understand that any pre-release
                and beta products we make available (“Beta Versions”) are still
                under development, may be inoperable or incomplete and are
                likely to contain more errors and bugs than generally available
                Products. We make no promises that any Beta Versions will ever
                be made generally available. In some circumstances, we may
                charge a fee in order to allow you to access Beta Versions, but
                the Beta Versions will still remain subject to this Section 11
                (No-Charge Products). All information regarding the
                characteristics, features or performance of Beta Versions
                constitutes Allied Media’s Confidential Information. To the
                maximum extent permitted by applicable law, we disclaim all
                obligations or liabilities with respect to No-Charge Products,
                including any Support and Maintenance, warranty, and indemnity
                obligations
              </p>
              <li>12. Restrictions.</li>
              <p>
                Except as otherwise expressly permitted in these End Customer
                T&Cs, you will not: (a) rent, lease, reproduce, modify, adapt,
                create derivative works of, distribute, sell, sublicense,
                transfer, or provide access to the Products to a third party,
                (b) use the Products for the benefit of any third party, (c)
                incorporate any Products into a product or service you provide
                to a third party, (d) interfere with any license key mechanism
                in the Products or otherwise circumvent mechanisms in the
                Products intended to limit your use, (e) reverse engineer,
                disassemble, decompile, translate, or otherwise seek to obtain
                or derive the source code, underlying ideas, algorithms, file
                formats or non-public APIs to any Products, except as permitted
                by law, (f) remove or obscure any proprietary or other notices
                contained in any Product, or (g) publicly disseminate
                information regarding the performance of the Products.
              </p>
              <li>13. Your Development of Add-Ons.</li>
              <p className="sub-para">13.1. License to Developer Guides.</p>
              <p>
                From time to time, Allied Media may publish SDK’s or API’s and
                associated guidelines (“Developer Guides”) to allow you to
                develop plugins, extensions, add-ons or other software products
                or services that interoperate or are integrated with the
                Products (“Add-Ons”). You may distribute your Add-Ons to third
                parties, but only for those Products permitted by Allied Media,
                and only in accordance with the Developer Guides.
              </p>
              <p>13.2. Conditions to Development of Add-Ons.</p>
              <p>
                Notwithstanding anything in these End Customer T&Cs to the
                contrary, Allied Media has no support, warranty, indemnification
                or other obligation or liability with respect to your Add-Ons or
                their combination, interaction or use with the Products. You
                shall indemnify, defend and hold us harmless from and against
                any and all claims, costs, damages, losses, liabilities and
                expenses (including reasonable attorneys’ fees and costs)
                arising out of or in connection with any claim brought against
                us by a third party relating to your Add-Ons (including but not
                limited to any representations or warranties you make about your
                Add-Ons) or your breach of this Section.
              </p>
              <li>14. License Certifications and Audits.</li>
              <p>
                At our request, you agree to provide a signed certification that
                you are using all Products pursuant to the terms of these End
                Customer T&Cs, including the Scope of Use. You agree to allow
                us, or our authorized agent, to audit your use of the Products.
                We will provide you with at least 10 days advance notice prior
                to the audit, and the audit will be conducted during normal
                business hours. We will bear all out-of-pocket costs that we
                incur for the audit, unless the audit reveals that you have
                exceeded the Scope of Use. You will provide reasonable
                assistance, cooperation, and access to relevant information in
                the course of any audit at your own cost. If you exceed your
                Scope of Use, we may invoice you for any past or ongoing
                excessive use, and you will pay the invoice promptly after
                receipt. This remedy is without prejudice to any other remedies
                available to Allied Media at law or equity or under these End
                Customer T&Cs. To the extent we are obligated to do so, we may
                share audit results with certain of our third-party licensors or
                assign the audit rights specified in this Section to such
                licensors.
              </p>
              <li>15. Ownership and Feedback.</li>
              <p>
                Products are made available on a limited license or access
                basis, and no ownership right is conveyed to you, irrespective
                of the use of terms such as “purchase” or “sale”. Allied Media
                and its licensors have and retain all right, title and interest,
                including all intellectual property rights, in and to the
                Products (including all No-Charge Products), their “look and
                feel”, any and all related or underlying technology, and any
                modifications or derivative works of the foregoing created by or
                for Allied Media, including without limitation as they may
                incorporate Feedback (“Allied Media Technology”). From time to
                time, you may choose to submit comments, information, questions,
                data, ideas, description of processes, or other information to
                Allied Media, including sharing Your Modifications or in the
                course of receiving Support and Maintenance (“Feedback”). Allied
                Media may in connection with any of its products or services
                freely use, copy, disclose, license, distribute and exploit any
                Feedback in any manner without any obligation, royalty or
                restriction based on intellectual property rights or otherwise.
                No Feedback will be considered your Confidential Information,
                and nothing in these End Customer T&Cs limits Allied Media’s
                right to independently use, develop, evaluate, or market
                products, whether incorporating Feedback or otherwise.
              </p>
              <li>16. Confidentiality.</li>
              <p>
                Except as otherwise set forth in these End Customer T&Cs, each
                party agrees that all code, inventions, know-how, business,
                technical and financial information disclosed to such party
                (“Receiving Party”) by the disclosing party (“Disclosing Party“)
                constitute the confidential property of the Disclosing Party
                (“Confidential Information“), provided that it is identified as
                confidential at the time of disclosure. Any Allied Media
                Technology and any performance information relating to the
                Products shall be deemed Confidential Information of Allied
                Media without any marking or further designation. Except as
                expressly authorized herein, the Receiving Party will hold in
                confidence and not use or disclose any Confidential Information.
                The Receiving Party’s nondisclosure obligation shall not apply
                to information which the Receiving Party can document: (i) was
                rightfully in its possession or known to it prior to receipt of
                the Confidential Information; (ii) is or has become public
                knowledge through no fault of the Receiving Party; (iii) is
                rightfully obtained by the Receiving Party from a third party
                without breach of any confidentiality obligation; or (iv) is
                independently developed by employees of the Receiving Party who
                had no access to such information. The Receiving Party may also
                disclose Confidential Information if so required pursuant to a
                regulation, law or court order (but only to the minimum extent
                required to comply with such regulation or order and with
                advance notice to the Disclosing Party). The Receiving Party
                acknowledges that disclosure of Confidential Information would
                cause substantial harm for which damages alone would not be a
                sufficient remedy, and therefore that upon any such disclosure
                by the Receiving Party the Disclosing Party shall be entitled to
                appropriate equitable relief in addition to whatever other
                remedies it might have at law. For the avoidance of doubt, this
                Section shall not operate as a separate warranty with respect to
                the operation of any Product
              </p>
              <li>17. Term and Termination.</li>
              <p>
                These End Customer T&Cs is in effect for as long as you have a
                valid License Term or Subscription Term (the “Term”), unless
                sooner terminated as permitted in these End Customer T&Cs.
                Either party may terminate these End Customer T&Cs before the
                expiration of the Term if the other party materially breaches
                any of the terms of these End Customer T&Cs and does not cure
                the breach within thirty (30) days after written notice of the
                breach. Either party may also terminate the End Customer T&Cs
                before the expiration of the Term if the other party ceases to
                operate, declares bankruptcy, or becomes insolvent or otherwise
                unable to meet its financial obligations. You may terminate
                these End Customer T&Cs at any time with notice to Allied Media,
                but you will not be entitled to any credits or refunds as a
                result of convenience termination for prepaid but unused
                Software, Hosted Services subscriptions, or Support and
                Maintenance. Except where an exclusive remedy may be specified
                in these End Customer T&Cs, the exercise by either party of any
                remedy, including termination, will be without prejudice to any
                other remedies it may have under these End Customer T&Cs, by
                law, or otherwise. Once the End Customer T&Cs terminates, you
                (and your Authorized Users) will no longer have any right to use
                or access any Products, or any information or materials that we
                make available to you under these End Customer T&Cs, including
                Allied Media Confidential Information. You are required to
                delete any of the foregoing from your systems as applicable
                (including any third-party systems operated on your behalf) and
                provide written certification to us that you have done so at our
                request. The following provisions will survive any termination
                or expiration of these End Customer T&Cs: Sections 7.7.3
                (Indemnity for Your Data), 10.3 (Payment), 10.4 (Taxes), 11
                (No-Charge Products) (disclaimers and use restrictions only), 12
                (Restrictions), 13.2 (Conditions to Development of Add-Ons), 14
                (License Certifications and Audits), 15 (Ownership and
                Feedback), 16 (Confidentiality), 17 (Term and Termination), 18.2
                (Warranty Disclaimer), 19 (Limitation of Liability), 21 (Third
                Party Vendor Products), 24 (Dispute Resolution), 25 (Export
                Restrictions), and 27 (General Provisions).
              </p>

              <li>18. Warranty and Disclaimer.</li>
              <p className="sub-para">18.1. Due Authority</p>
              <p>
                Each party represents and warrants that it has the legal power
                and authority to enter into these End Customer T&Cs, and that,
                if you are an entity, these End Customer T&Cs and each Order is
                entered into by an employee or agent of such party with all
                necessary authority to bind such party to the terms and
                conditions of these End Customer T&Cs.
              </p>
              <p className="sub-para">18.2. WARRANTY DISCLAIMER</p>
              <p>
                ALL PRODUCTS ARE PROVIDED “AS IS,” AND ALLIED MEDIA AND ITS
                SUPPLIERS EXPRESSLY DISCLAIM ANY AND ALL WARRANTIES AND
                REPRESENTATIONS OF ANY KIND, INCLUDING ANY WARRANTY OF
                NON-INFRINGEMENT, TITLE, FITNESS FOR A PARTICULAR PURPOSE,
                FUNCTIONALITY, OR MERCHANTABILITY, WHETHER EXPRESS, IMPLIED, OR
                STATUTORY. YOU MAY HAVE OTHER STATUTORY RIGHTS, BUT THE DURATION
                OF STATUTORILY REQUIRED WARRANTIES, IF ANY, SHALL BE LIMITED TO
                THE SHORTEST PERIOD PERMITTED BY LAW. ALLIED MEDIA SHALL NOT BE
                LIABLE FOR DELAYS, INTERRUPTIONS, SERVICE FAILURES AND OTHER
                PROBLEMS INHERENT IN USE OF THE INTERNET AND ELECTRONIC
                COMMUNICATIONS OR OTHER SYSTEMS OUTSIDE THE REASONABLE CONTROL
                OF ALLIED MEDIA. TO THE MAXIMUM EXTENT PERMITTED BY LAW, NEITHER
                ALLIED MEDIA NOR ANY OF ITS THIRD PARTY SUPPLIERS MAKES ANY
                REPRESENTATION, WARRANTY OR GUARANTEE AS TO THE RELIABILITY,
                TIMELINESS, QUALITY, SUITABILITY, TRUTH, AVAILABILITY, ACCURACY
                OR COMPLETENESS OF ANY PRODUCTS OR ANY CONTENT THEREIN OR
                GENERATED THEREWITH, OR THAT: (A) THE USE OF ANY PRODUCTS WILL
                BE SECURE, TIMELY, UNINTERRUPTED OR ERROR-FREE; (B) THE PRODUCTS
                WILL OPERATE IN COMBINATION WITH ANY OTHER HARDWARE, SOFTWARE,
                SYSTEM, OR DATA; (C) THE PRODUCTS (OR ANY PRODUCTS, SERVICES,
                INFORMATION, OR OTHER MATERIAL PURCHASED OR OBTAINED BY YOU
                THROUGH THE PRODUCTS) WILL MEET YOUR REQUIREMENTS OR
                EXPECTATIONS); (D) ANY STORED DATA WILL BE ACCURATE OR RELIABLE
                OR THAT ANY STORED DATA WILL NOT BE LOST OR CORRUPTED; (E)
                ERRORS OR DEFECTS WILL BE CORRECTED; OR (F) THE PRODUCTS (OR ANY
                SERVER(S) THAT MAKE A HOSTED SERVICE AVAILABLE) ARE FREE OF
                VIRUSES OR OTHER HARMFUL COMPONENTS.
              </p>
              <li>19. Limitation of Liability.</li>
              <p>
                NEITHER PARTY (NOR ITS SUPPLIERS) SHALL BE LIABLE FOR ANY LOSS
                OF USE, LOST OR INACCURATE DATA, FAILURE OF SECURITY MECHANISMS,
                INTERRUPTION OF BUSINESS, COSTS OF DELAY OR ANY INDIRECT,
                SPECIAL, INCIDENTAL, RELIANCE, OR CONSEQUENTIAL DAMAGES OF ANY
                KIND (INCLUDING LOST PROFITS), REGARDLESS OF THE FORM OF ACTION,
                WHETHER IN CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT
                LIABILITY OR OTHERWISE, EVEN IF INFORMED OF THE POSSIBILITY OF
                SUCH DAMAGES IN ADVANCE. NEITHER PARTY’S AGGREGATE LIABILITY TO
                THE OTHER SHALL EXCEED THE AMOUNT ACTUALLY PAID BY YOU TO US FOR
                PRODUCTS AND SUPPORT AND MAINTENANCE IN THE 12 MONTHS
                IMMEDIATELY PRECEDING THE CLAIM. NOTWITHSTANDING ANYTHING ELSE
                IN THESE END CUSTOMER T&CS, OUR AGGREGATE LIABILITY TO YOU IN
                RESPECT OF NO-CHARGE PRODUCTS SHALL BE US$20. THIS SECTION 19
                (LIMITATION OF LIABILITY) SHALL NOT APPLY TO (1) AMOUNTS OWED BY
                YOU UNDER ANY ORDERS, (2) EITHER PARTY’S EXPRESS INDEMNIFICATION
                OBLIGATIONS IN THESE END CUSTOMER T&CS, OR (3) YOUR BREACH OF
                SECTION 12 (RESTRICTIONS) OR SECTION 2 (COMBINING THE PRODUCTS
                WITH OPEN SOURCE SOFTWARE) OF THIRD-PARTY CODE IN ALLIED MEDIA
                PRODUCTS). TO THE MAXIMUM EXTENT PERMITTED BY LAW, NO SUPPLIERS
                OF ANY THIRD-PARTY COMPONENTS INCLUDED IN THE PRODUCTS WILL BE
                LIABLE TO YOU FOR ANY DAMAGES WHATSOEVER. The parties agree that
                the limitations specified in this Section 19 (Limitation of
                Liability) will survive and apply even if any limited remedy
                specified in these End Customer T&Cs is found to have failed of
                its essential purpose
              </p>
              <li>20. IP Indemnification by Allied Media.</li>
              <p>
                We will defend you against any claim brought against you by a
                third party alleging that a Product, when used as authorized
                under these End Customer T&Cs, infringes a United States or
                European Union patent or registered copyright (a “Claim”), and
                we will indemnify you and hold you harmless against any damages
                and costs finally awarded by a court of competent jurisdiction
                or agreed to settlement by Allied Media (including reasonable
                attorneys’ fees) arising out of a Claim, provided that we have
                received from you: (a) prompt written notice of the claim (but
                in any event notice in sufficient time for us to respond without
                prejudice); (b) reasonable assistance in the defense and
                investigation of the claim, including providing us a copy of the
                claim and all relevant evidence in your possession, custody or
                control; and (c) the exclusive right to control and direct the
                investigation, defense, and settlement (if applicable) of the
                claim. If your use of a Product is (or in our opinion is likely
                to be) enjoined, if required by settlement, or if we determine
                such actions are reasonably necessary to avoid material
                liability, we may, at our option and in our discretion: (i)
                procure a license for your continued use of the Product in
                accordance with these End Customer T&Cs; (ii) substitute a
                substantially functionally similar Product; or (iii) terminate
                your right to continue using the Product and refund, in the case
                of Software, the license fee paid by you as reduced to reflect a
                three year straight-line depreciation from the license purchase
                date, and in the case of a Hosted Service, any prepaid amounts
                for the terminated portion of the Subscription Term. Allied
                Media’s indemnification obligations above do not apply: (1) if
                the total aggregate fees received by Allied Media with respect
                to your license to Software or subscription to Hosted Services
                in the 12 month period immediately preceding the claim is less
                than US$50,000; (2) if the Product is modified by any party
                other than Allied Media, but solely to the extent the alleged
                infringement is caused by such modification; (3) if the Product
                is used in combination with any non-Allied Media product,
                software or equipment, but solely to the extent the alleged
                infringement is caused by such combination; (4) to unauthorized
                use of Products; (5) to any Claim arising as a result of (y)
                Your Data (or circumstances covered by your indemnification
                obligations in Section 7.7.3 (Indemnity for Your Data)) or (z)
                any third-party deliverables or components contained with the
                Products; (6) to any unsupported release of the Software; or (7)
                if you settle or make any admissions with respect to a claim
                without Allied Media’s prior written consent. THIS SECTION 20
                (IP INDEMNIFICATION BY ALLIED MEDIA) STATES OUR SOLE LIABILITY
                AND YOUR EXCLUSIVE REMEDY FOR ANY INFRINGEMENT OF INTELLECTUAL
                PROPERTY RIGHTS IN CONNECTION WITH ANY PRODUCT OR OTHER ITEMS
                PROVIDED BY ALLIED MEDIA UNDER THESE END CUSTOMER T&CS.
              </p>
              <li>21. Third Party Vendor Products.</li>
              <p>
                Allied Media or third parties may from time to time make
                available to you third-party products or services, including but
                not limited to add-ons and plugins as well as implementation,
                customization, training, and other consulting services. If you
                procure any of these third-party products or services, you do so
                under a separate agreement (and exchange of data) solely between
                you and the third-party vendor. Allied Media does not warrant or
                support non-Allied Media products or services, whether or not
                they are designated by Allied Media as “verified” or otherwise
                and disclaims all liability for such products or services. If
                you install or enable any third-party products or services for
                use with Allied Media products, you acknowledge that Allied
                Media may allow the vendors of those products and services to
                access Your Data as required for the interoperation and support
                of such add-ons with the Allied Media products. Allied Media
                shall not be responsible for any disclosure, modification or
                deletion of Your Data resulting from any such access by third
                party add-on vendors.
              </p>
              <li>22. Publicity Rights.</li>
              <p>
                We may identify you as an Allied Media customer in our
                promotional materials. You may request that we stop doing so by
                submitting an email to sales@alliedmedia.com at any time. Please
                note that it may take us up to 30 days to process your request.
              </p>
              <li>23. Improving Our Products.</li>
              <p>
                We are always striving to improve our Products. In order to do
                so, we need to measure, analyze, and aggregate how users
                interact with our Products, such as usage patterns and
                characteristics of our user base. We collect and use analytics
                data regarding the use of our Products as described in our
                Privacy Policy.
              </p>
              <li>24. Dispute Resolution.</li>
              <p className="sub-para">24.1. Injunctive Relief: Enforcement</p>
              <p>
                Notwithstanding the provisions of Section 24.1 (Dispute
                Resolution; Arbitration), nothing in these End Customer T&Cs
                shall prevent either party from seeking injunctive relief with
                respect to a violation of intellectual property rights,
                confidentiality obligations, or enforcement or recognition of
                any award or order in any appropriate jurisdiction.
              </p>
              <p className="sub-para">
                24.2. Exclusion of UN Convention and UCITA.
              </p>
              <p>
                The terms of the United Nations Convention on Contracts for the
                Sale of Goods do not apply to these End Customer T&Cs. The
                Uniform Computer Information Transactions Act (UCITA) shall not
                apply to these End Customer T&Cs regardless of when or where
                adopted.
              </p>
              <li>25. Export Restrictions.</li>
              <p>
                The Products are subject to export restrictions by the United
                States government and import restrictions by certain foreign
                governments, and you agree to comply with all applicable export
                and import laws and regulations in your use of the Products. You
                shall not (and shall not allow any third-party to) remove or
                export from the United States or allow the export or re-export
                of any part of the Products or any direct product thereof: (a)
                into (or to a national or resident of) any embargoed or
                terrorist-supporting country; (b) to anyone on the U.S. Commerce
                Department’s Table of Denial Orders or U.S. Treasury
                Department’s list of Specially Designated Nationals; (c) to any
                country to which such export or re-export is restricted or
                prohibited, or as to which the United States government or any
                agency thereof requires an export license or other governmental
                approval at the time of export or re-export without first
                obtaining such license or approval; or (d) otherwise in
                violation of any export or import restrictions, laws or
                regulations of any United States or foreign agency or authority.
                You represent and warrant that (i) you are not located in, under
                the control of, or a national or resident of any such prohibited
                country or on any such prohibited party list and (ii) that none
                of Your Data is controlled under the US International Traffic in
                Arms Regulations. The Products are restricted from being used
                for the design or development of nuclear, chemical, or
                biological weapons or missile technology without the prior
                permission of the United States government.
              </p>
              <li>26. Changes to these End Customer T&Cs.</li>
              <p>
                We may update or modify these End Customer T&Cs from time to
                time, including any referenced policies and other documents. If
                a revision meaningfully reduces your rights, we will use
                reasonable efforts to notify you (by, for example, sending an
                email to the billing or technical contact you designate in the
                applicable Order, posting on our blog, through your Allied Media
                account, or in the Product itself). If we modify the End
                Customer T&Cs during your License Term or Subscription Term, the
                modified version will be effective upon your next renewal of a
                License Term, Support and Maintenance term, or Subscription
                Term, as applicable. In this case, if you object to the updated
                End Customer T&Cs, as your exclusive remedy, you may choose not
                to renew, including cancelling any terms set to auto-renew. With
                respect to No-Charge Products, accepting the updated End
                Customer T&Cs is required for you to continue using the
                No-Charge Products. You may be required to click through the
                updated End Customer T&Cs to show your acceptance. If you do not
                agree to the updated End Customer T&Cs after it becomes
                effective, you will no longer have a right to use No-Charge
                Products. For the avoidance of doubt, any Order is subject to
                the version of the End Customer T&Cs in effect at the time of
                the Order
              </p>
              <li>27. General Provisions.</li>
              <p>
                Any notice under these End Customer T&Cs must be given in
                writing. We may provide notice to you via email or through your
                account. Our notices to you will be deemed given upon the first
                business day after we send it. You may provide notice to us by
                post to: Allied Media Incorporated, ATTN: Legal Department, 2
                Robert Speck Parkway, Suite 750, Mississauga, Ontario, L4Z 1H8,
                Canada with a copy via email to notices@alliedmedia.com. Your
                notices to us will be deemed given upon our receipt. Neither
                party shall be liable to the other for any delay or failure to
                perform any obligation under these End Customer T&Cs (except for
                a failure to pay fees) if the delay or failure is due to
                unforeseen events which are beyond the reasonable control of
                such party, such as a strike, blockade, war, act of terrorism,
                riot, natural disaster, failure or diminishment of power or
                telecommunications or data networks or services, or refusal of a
                license by a government agency. You may not assign these End
                Customer T&Cs without our prior written consent. We will not
                unreasonably withhold our consent if the assignee agrees to be
                bound by the terms and conditions of these End Customer T&Cs. We
                may assign our rights and obligations under these End Customer
                T&Cs (in whole or in part) without your consent. The Products
                are commercial computer software. If you are an agency,
                department, or other entity of the United States Government, the
                use, duplication, reproduction, release, modification,
                disclosure, or transfer of the Products, or any related
                documentation of any kind, including technical data and manuals,
                is restricted by the terms of these End Customer T&Cs in
                accordance with Federal Acquisition Regulation 12.212 for
                civilian purposes and Defense Federal Acquisition Regulation
                Supplement 227.7202 for military purposes. The Products were
                developed fully at private expense. All other use is prohibited.
                These End Customer T&Cs is the entire agreement between you and
                Allied Media relating to the Products and supersedes all prior
                or contemporaneous oral or written communications, proposals and
                representations with respect to the Products or any other
                subject matter covered by these End Customer T&Cs. If any
                provision of these End Customer T&Cs is held to be void,
                invalid, unenforceable or illegal, the other provisions shall
                continue in full force and effect. These End Customer T&Cs may
                not be modified or amended by you without our written agreement
                (which may be withheld in our complete discretion without any
                requirement to provide any explanation). As used herein,
                “including” (and its variants) means “including without
                limitation” (and its variants). No failure or delay by the
                injured party to these End Customer T&Cs in exercising any
                right, power or privilege hereunder shall operate as a waiver
                thereof, nor shall any single or partial exercise thereof
                preclude any other or further exercise thereof or the exercise
                of any right, power or privilege hereunder at law or equity. The
                parties are independent contractors. These End Customer T&Cs
                shall not be construed as constituting either party as a partner
                of the other or to create any other form of legal association
                that would give on party the express or implied right, power or
                authority to create any duty or obligation of the other party.
              </p>
              <p style={{ marginTop: "15px" }}>
                <strong>
                  Confirmed on behalf of Choctaw Nation Confirmed on behalf of
                  Allied Media By: Name: Mike Everson<br></br> Name: Dheeraj
                  Joshi Title: Director Enterprise Applications Title: Vice
                  President, Solution Delivery
                </strong>
              </p>
            </ol>
          </section>
        </Container>
      </section>
    </div>
  );
}
