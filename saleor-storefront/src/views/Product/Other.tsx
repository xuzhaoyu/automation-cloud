import * as React from "react";
import { FormattedMessage } from "react-intl";

import { ProductList } from "@components/organisms";

import { ProductDetails_product_category_products_edges } from "./gqlTypes/ProductDetails";

const OtherProducts: React.FC<{
  products: ProductDetails_product_category_products_edges[];
  id: string;
}> = ({ products, id }) => (
  <div className="product-page__other-products">
    <div className="container">
      <h4 className="product-page__other-products__title">
        <FormattedMessage defaultMessage="Other products in this category" />
      </h4>

      <ProductList products={products.map(({ node: item }) => {
        if (item.id !== id) {
          return item
        }
      })} />

    </div>
  </div>
);

export default OtherProducts;
