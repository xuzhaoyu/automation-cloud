import "./scss/index.scss";
import "./scss/index.css";
import * as React from "react";
import NewsMailer from "./NewsMailer";
import Testimonials from "./Testimonials";
import Howitworks from "./Howitworks";
import { Loader } from "../../components";
// import { Loader, ProductsFeatured } from "../../components";
import Legal from "../../images/legalC.png"
import IT from "../../images/itC.png"
import Finance from "../../images/financeC.png"
import supply from "../../images/supplyChainC.png"
import compliance from "../../images/GRCC.png"
import cybersecurity from "../../images/cyber-securityC.png"

import {
  ProductsList_categories,
  ProductsList_shop,
  ProductsList_shop_homepageCollection_backgroundImage,
} from "./gqlTypes/ProductsList";
import { useState } from 'react';
import CarouselBanner from "./CarouselBanner";
import IntroSection from './IntroSection'
import { structuredData } from "../../core/SEO/Homepage/structuredData";
var arrImg = [supply, Legal, IT, compliance, Finance, cybersecurity];
import AllProducts from './Products';

const Page: React.FC<{
  loading: boolean;
  categories: ProductsList_categories;
  backgroundImage: ProductsList_shop_homepageCollection_backgroundImage;
  shop: ProductsList_shop;


}> = ({ loading, categories, backgroundImage, shop }) => {
  const categoriesExist = () => {
    return categories && categories.edges && categories.edges.length > 0;
  };

  const [active, setActive] = useState(0);
  const [selected, setSelected] = useState("Supply Chain");
  const filter = (input, index) => {
    setSelected(input);
    setActive(index);
  }

  return (
    <>

      <script className="structured-data-list" type="application/ld+json">
        {structuredData(shop)}
      </script>


      <div>{loading && !categories ? <Loader /> : <CarouselBanner />}</div>
      <div style={{ marginTop: "35px" }}>
        {categoriesExist() && (
          <div className="home-page__categories">
            <div style={{ margin: "0 13%", marginTop: "10px" }}>

              <h1 style={{ textAlign: "center", marginBottom: "30px", fontSize: "32px" }}><strong>Categories</strong></h1>

              <div className="categories">
                {

                  categories.edges.map(({ node: category }, index) => (
                    <div key={category.id}>
                      <div className={`${index == active ? "categories_item active" : "categories_item"}`} onClick={() => filter(category.name, index)} onMouseEnter={() => (filter(category.name, index))}>

                        <img src={arrImg[index]} alt="categories" style={{ width: "4rem" }} />
                        <h4 className="category_heading">
                          {category.name}
                        </h4>
                      </div>
                    </div>
                  ))
                }
              </div>
            </div>
          </div>
        )}
      </div>


      {/* <ProductsFeatured
        title={intl.formatMessage({ defaultMessage: "Shop By Automation" })}
      /> */}
      <div style={{ margin: "0px 13%" }}>
        <section id="all-products" style={{ marginTop: "80px" }}>
          <AllProducts selected={selected} />
        </section>
        <section id="howitworks" style={{ marginTop: "80px" }}>
          <Howitworks />
        </section>
        <section id="CategoryIntro" style={{ marginTop: "80px" }}>
          <IntroSection />
        </section>
        <section id="#testimonials" style={{ marginTop: "80px" }}>
          <Testimonials />
        </section>

      </div>
      <NewsMailer />

    </>
  );
};

export default Page;
