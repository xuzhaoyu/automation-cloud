import React from 'react';
import "./scss/index.css";
import { Row, Col } from "react-bootstrap";
import one from "../../images/number1.png"
import two from "../../images/number2.png"
import three from "../../images/number3.png"
import howitworks from '../../images/howitworks.png'



function Howitworks() {

    return (
        <>

            <div className="howItWorks">
                <div className="left">
                    <img src={howitworks} alt="how it works" />
                </div>
                <div className="right">
                    <h2>How it works?</h2>
                    <div className="rightcontainer">
                        <div className="item">
                            <div className="avatar">
                                <img src={one} alt="one" />
                            </div>
                            <div className="text">
                                <h3>Hire</h3>
                                <p>Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet</p>

                            </div>
                        </div>
                        <div className="item">
                            <div className="avatar">
                                <img src={two} alt="two" />
                            </div>
                            <div className="text">
                                <h3>Induct</h3>
                                <p>Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet</p>

                            </div>
                        </div>
                        <div className="item">
                            <div className="avatar">
                                <img src={three} alt="three" />
                            </div>
                            <div className="text">
                                <h3>Operate</h3>
                                <p>Lorem ipsum dolor sit amet.Lorem ipsum dolor sit amet</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/* <div>
              <h1 style={{textAlign:"center",marginBottom:"2rem"}}>How it Works ? </h1>
              <div>
                    <Row>
                      <Col lg={4} md={4} xs={12}>
                      <div style={{textAlign:'center'}}>
                      <img className="responsive-number" style={{width:"7rem",marginBottom:"1rem"}} src={one} alt="cannot render"/>
                      <h2 className="count-text">Hire</h2>
                      </div>
                      </Col>

                      <Col lg={4} md={4} xs={12}>
                      <div style={{textAlign:'center'}}>
                      <img className="responsive-number" style={{width:"7rem",marginBottom:"1rem"}} src={two} alt="cannot render"/>
                      <h2 className="count-text">Induct</h2>
                      </div>
                      </Col>

                      <Col lg={4} md={4} xs={12}>
                      <div style={{textAlign:'center'}}>
                      <img className="responsive-number" style={{width:"7rem",marginBottom:"1rem"}} src={three} alt="cannot render"/>
                      <h2 className="count-text">Operate</h2>
                      </div>
                      </Col>
                      </Row>
              </div> 
          </div>
              */}


        </>

    )
}

export default Howitworks;
