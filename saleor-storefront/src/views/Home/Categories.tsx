import React from 'react';
import "./scss/index.css";
import {Row,Col} from "react-bootstrap";
import Legal from "../../images/Legal.png"
import IT from "../../images/IT.png"
import Finance from "../../images/Finance.png"
import supply  from "../../images/supply.png"
import compliance from "../../images/compliance.png"



function Howitworks() {

    return (
        <>
         <section id="#How-it-works">
          <div>
              <h1 style={{textAlign:"center",marginBottom:"20px"}}>Categories </h1>
              <div style={{marginTop:'35px'}}>
                    <Row>
                      {/* <Col lg={4} md={4} xs={6}> */}
                      <Col>
                      <div style={{textAlign:'center'}}>
                      <img style={{height:"60px",width:"60px"}} src={Legal} alt="cannot render"/>
                      <h2 className="count-text" style={{marginTop:'15px'}}>Legal</h2>
                      </div>
                      </Col>
                      <Col >
                      <div style={{textAlign:'center'}}>
                      <img style={{height:"60px",width:"60px"}} src={IT} alt="cannot render"/>
                      <h2 className="count-text" style={{marginTop:'15px'}}>IT</h2>
                      </div>
                      </Col>
                      <Col >
                      <div style={{textAlign:'center'}}>
                      <img style={{height:"60px",width:"60px"}} src={Finance} alt="cannot render"/>
                      <h2 className="count-text" style={{marginTop:'15px'}}>Finanace</h2>
                      </div>
                      </Col>

                      <Col >
                      <div style={{textAlign:'center'}}>
                      <img style={{height:"60px",width:"60px"}} src={compliance} alt="cannot render"/>
                      <h2 className="count-text" style={{marginTop:'15px'}}>GRC</h2>
                      </div>
                      </Col>

                      <Col >
                      <div style={{textAlign:'center'}}>
                      <img style={{height:"60px",width:"60px"}} src={supply} alt="cannot render"/>
                      <h2 className="count-text" style={{marginTop:'15px'}}>Supply Chain</h2>
                      </div>
                      </Col>
                      </Row>
              </div>
          </div>
          
      </section>
        </>
    )
}

export default Howitworks;
