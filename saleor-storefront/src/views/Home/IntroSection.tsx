import React from 'react';
import "./scss/index.css";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import shopByAutomation1 from "../../images/shopByAutomation1.png";
import shopByAutomation2 from "../../images/shopByAutomation2.png";
import shopByAutomation3 from "../../images/shopByAutomation3.png";
import shopByAutomation4 from "../../images/shopByAutomation4.png";
import shopByAutomation5 from "../../images/shopByAutomation5.png";
import shopByAutomation6 from "../../images/shopByAutomation6.png";

export default function IntroSection() {
    var settings = {
       
        slidesToShow: 1,
        slideToScroll:1,
        arrows:false,
        autoplay:true,
        autoplaySpeed:3000,
       

    };
    return (
      <div className="introSection">
          <div className="introSection__left">
              <h3>Shop by Automation</h3>
              <h1>Lorem ipsum dolor sit amet architecto.</h1>
              <button>View More</button>

          </div>
          <div className="introSection__right">
              <Slider  {...settings}>
              <div className="slider">
                <img src={shopByAutomation1} alt="Supply Chain" />
              </div>
              <div className="slider">
                <img src={shopByAutomation2} alt="Legal" />
              </div>
              <div className="slider">
                <img src={shopByAutomation3} alt="GRC" />
              </div>
              <div className="slider">
                <img src={shopByAutomation4} alt="GRC" />
              </div>
              <div className="slider">
                <img src={shopByAutomation5} alt="GRC" />
              </div>
              <div className="slider">
                <img src={shopByAutomation6} alt="GRC" />
              </div>
              </Slider >
            
          </div>
      </div>
    );
};
