/* tslint:disable */
/* eslint-disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ProductsList
// ====================================================


export interface MinimalVariantPrice {
    amount: number;
    currency: string;
}
export interface Products_edges_node_thumbnail {
    __typename: "Image";
    url: string;
    alt: string;


}
export interface Category {
    __typename: "category"
    id: string;
    name: string;
}
export interface thumbnail2x {
    __typename: "Image";
    /**
     * The URL of the image.
     */
    url: string;
}
export interface Products_edges_node {
    id: string;
    name: string;
    minimalVariantPrice: MinimalVariantPrice;
    thumbnail: Products_edges_node_thumbnail;
    category: Category;
    thumbnail2x: thumbnail2x;
}
export interface Products_edges {
    __typename: "ProducteEdge";
    /**
     * The item at the end of the edge.
     */
    node: Products_edges_node;
}

export interface Products_products {

    edges: Products_edges[];
}
export interface Products {
    products: Products_products;
}
