import "./scss/index.scss";
import "./scss/index.css";
import * as React from "react";

// import ProductListItem from '../../components/ProductListItem';
import { TypedProductQuery } from "./queries";
import { Thumbnail } from "@components/molecules";
import { generateProductUrl } from "../../core/utils";
import { Link } from "react-router-dom";
import { Loader } from "../../components";

interface ProductCategoryProps {
  selected: string;
}

const AllProducts: React.FC<ProductCategoryProps> = ({ selected }) => {
  return (
    <div className="products">
      <div className="products-featured__header">
        <h3>{`Robots for ${selected}`}</h3>
        <button>View All</button>
      </div>
      <TypedProductQuery alwaysRender displayLoader={false} errorPolicy="all">
        {({ data, loading }) => {
          return (
            <>
              <div className="products_container">
                {loading ? (
                  <Loader />
                ) : (
                  data.products.edges.map(({ node: item }) => {
                    if (item.category.name === selected) {
                      return (
                        // <>
                        //     {console.log(item, "item-", index)}
                        // </>
                        <Link
                          to={generateProductUrl(item.id, item.name)}
                          key={item.id}
                        >
                          <div className="product-list-item" key={item.id}>
                            <div className="product-list-item__image">
                              <Thumbnail source={item} />
                            </div>
                            <div className="product-list-item__details">
                              <h4 className="product-list-item__title">
                                {item.name}
                              </h4>
                              <p className="product-list-item__category">
                                {item.category?.name}
                              </p>
                              <p className="product-list-item__price">{`$${item.minimalVariantPrice?.amount}`}</p>
                            </div>
                          </div>
                        </Link>
                      );
                    }
                  })
                )}
              </div>
            </>
          );
        }}
      </TypedProductQuery>
    </div>
  );
};

export default AllProducts;

// data.products.edges.map(({ node: item }) => (
// <Link
//     to={generateProductUrl(item.id, item.name)}
//     key={item.id}
// >
//     <div className="product-list-item" key={item.id}>
//         <div className="product-list-item__image">
//             <Thumbnail source={item} />
//         </div>
//         <div className="product-list-item__details">
//             <h4 className="product-list-item__title">{item.name}</h4>
//             <p className="product-list-item__category">{item.category?.name}</p>
//             <p className="product-list-item__price">{`$${item.minimalVariantPrice?.amount}`}</p>
//         </div>

//     </div>
// </Link>
// )
// )
