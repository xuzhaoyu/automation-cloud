import React from 'react'
import "./scss/index.css"
import {Carousel,Row,Col} from "react-bootstrap"
// import image1 from "../../images/hacker-woman-remake.jpg"
// import image2 from "../../images/online-web-design-remake.jpg"
// import image3 from "../../images/hooded-computer-remake.jpg"
// import image4 from "../../images/coding-man-remake.jpg"

import image1 from "../../images/banner1.jpg"
import image2 from "../../images/banner2.jpg"
import image3 from "../../images/banner3.jpg"
import image4 from "../../images/banner2.jpg"
import bannerfix from '../../images/topBanner.png';

import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

export default function CarouselBanner() {
  var settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay:true,
    autoplaySpeed: 3000,
    
  };
    return (
      <><div className="carousel">
        <div className="carousel__left">
          <h1>Our Company is Commited earning your trust
            by providing the expertise
          </h1>
          <div className="button" style={{  width:"25rem"}}>
          <button>Get Started</button>
        </div>

      </div><div className="carousel__right">
          <img src={bannerfix} alt="bannerImge" />
        </div>
        
      </div>
      
      </> 
    )
}

{/* <Slider {...settings}>
          <div className="carousel_item"><img src={image1} alt="image1" /></div>
          <div className="carousel_item"><img src={image2} alt="image2" /></div>
          <div className="carousel_item"><img src={image3} alt="image3" /></div>
          <div className="carousel_item"><img src={image4} alt="image4" /></div>
         
        </Slider> */}