import gql from "graphql-tag";
import { TypedQuery } from "../../core/queries";
import { ProductsList } from "./gqlTypes/ProductsList";
import { Products } from './gqlTypes/Products';

export const homePageQuery = gql`
  query ProductsList {
    shop {
      description
      name
      homepageCollection {
        id
        backgroundImage {
          url
        }
        name
      }
    }
    categories(level: 0, first: 9) {
      edges {
        node {
          id
          name
          backgroundImage {
            url
          }
        }
      }
    }
   
  }
  
`;
export const productQuery = gql`
query GetProducts {
  products(first: 40) {
    edges {
      node {
        id
        name
        minimalVariantPrice {
          amount
          currency
        }
        thumbnail {
          url
          alt
        }
        thumbnail2x: thumbnail(size: 510) {
          url
        }
        category {
          id
          name
        }
       
      }
    }
  }
}
`;


export const TypedHomePageQuery = TypedQuery<ProductsList, {}>(homePageQuery);
export const TypedProductQuery = TypedQuery<Products, {}>(productQuery);
