import React,{useState} from 'react';
import {Carousel,Row,Container,Col} from 'react-bootstrap'
// import {account1} from './Account.json'
// import image from "../../images/account2.jpg"
import './scss/index.css'
import account from '../../images/account2.jpg'
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";


const Testimonials: React.FC=()=>{
  var settings = {
    centerMode: true,
    centerPadding: '60px',
     slidesToShow: 3,
     slideToScroll:1,
    responsive: [
      {
        breakpoint: 900,
        settings: {
          
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          
          centerMode: true,
          centerPadding: '40px',
          slidesToShow: 1
        }
      }
    ]
    
  };
    const testimonials=[
      {
        name:'Mr Taylor',
        imagePath:"",
        text:" an arrangement of buildings on a city block, or styles of clothing. It is a coherent set of signs that transmits some kind of informative message.",
      },
      {
        name:'Mr James',
        imagePath:"",
        text:" on the deck of ship on a city block, or styles of clothing. It is a coherent set of signs that transmits some kind of informative message.",
      },
      {
        name:'Mr Jonathon',
        imagePath:"",
        text:" rather moving out of buildings on a city block, or styles of clothing. It is a coherent set of signs that transmits some kind of informative message.",
      },
      {
        name:'Mr Jon Admut',
        imagePath:"",
        text:" an arrangement of buildings on a city block, or styles of clothing. It is a coherent set of signs that transmits some kind of informative message.",
      },
    ];
    
        
      
        return (
           
              <div className="testimonials">
                
                <h1 style={{textAlign:"center",marginBottom:"3rem"}}>Testimonials</h1>
              <Slider {...settings}>
                {
                testimonials.map((item,index)=>(
                 
                  <div className="testimonials__item" style={{width:"30%"}} key={index}>
                    <img src={item.imagePath?item.imagePath: account} alt="image1" />
                    <h3>{item.name}</h3>
                    <p className="text">
                    {item.text}
                    </p>
                  </div>
                  
                
                ))
                }
               
              </Slider>
              </div>
            
             
          
         
        );
      
}
export default Testimonials;

 {/* <Row>
                <h1 style={{textAlign:"center"}}>Testimonials</h1>
          
                    <Carousel>
                   <Carousel.Item>
                  <div style={{display:"flex",justifyContent:'center',padding:"85px"}}>
                   <img
                     className="f-block"
                     src={account}
                     alt="First slide"
                   />
                  
                     <p style={{color:"black",padding:"10px"}}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</p> 
                     </div>
                 </Carousel.Item>
                 <Carousel.Item>  
                 <div style={{display:"flex",justifyContent:'center',padding:"95px"}}>
                   <img
                     className="f-block"
                     src={account}
                     alt="Second slide"
                   />
                      <p style={{color:"black",padding:"10px"}}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown type specimen book. It has survived not</p>
                    </div>
                 </Carousel.Item>
                 <Carousel.Item>
                 <div style={{display:"flex",justifyContent:'center',padding:"95px"}}>
                 <img
                     className="f-block"
                     src={account}
                     alt="Third slide"
                   />
              
                      <p style={{color:"black",padding:"10px"}}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not</p>
                   </div> 
                 </Carousel.Item>
              
           </Carousel>  
        
                  
          </Row> */}
          