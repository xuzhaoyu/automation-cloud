import React,{useEffect} from "react";

import { useAuth } from "@saleor/sdk";
import { DemoBanner, Loader } from "@components/atoms";
import { demoMode } from "@temp/constants";
import {
  Footer,
  MainMenu,
  MetaConsumer,
  OverlayManager,
  OverlayProvider,
} from "../components";
import ShopProvider from "../components/ShopProvider";
import { BrowserRouter as Router,Route, Switch } from "react-router-dom";
import "../globalStyles/scss/index.scss";
// import { Routes } from "./routes";
import Notifications from "./Notifications";
import  {HomePage}  from "../views/Home";
import { ProductPage } from "../views/Product";
import { SearchPage } from "../views/Search";
import  View from "../components/Test/View";
import About from "../components/About/About";

import { CartPage, CheckoutPage, PasswordReset, ThankYouPage } from "@pages";
import { CheckoutLogin, NotFound } from "../components";
import UserAccount, * as accountPaths from "../userAccount/routes";
import { OrderDetails } from "../userAccount/views";
import { Account, AccountConfirm } from "../views/Account";
import { ArticlePage } from "../views/Article";
import { CategoryPage } from "../views/Category";
import { CollectionPage } from "../views/Collection";
import Copy from "../components/Copyright/Copy";
import AutomationPage  from "../components/AutomationCart/AutomationPage";
import Associates from "../components/Associates/associates";
import Test1 from "../components/Test/View";
import { useSelector } from "react-redux";
import Navbar from '../components/Associates/Navbar'

// import { ApolloClient,ApolloProvider,InMemoryCache,HttpLink,from } from "@apollo/client";
// import {onError} from "@apollo/client/link/error";
// import Products from "../components/GET_PRODUCTS/Products"

// import Dumb from "../components/Dummy/Dumb"
// import Jerry from "../components/Jerryify/Jerry"
// import{ Newpage} from "../components/Newpage/Newpage";

import * as paths from "./paths";

// const errorLink= onError(({graphQLErrors,networkError})=>{
//   if(graphQLErrors){
//     graphQLErrors.map(({message,path})=>{
//       alert(`GRAPH ERROR  ${message}`)
//     })
//   }
// })
// const link= from([
//   errorLink,
//   new HttpLink({uri:"http://localhost:8002/graphql"})
// ])

// const client=new ApolloClient({
//   cache: new InMemoryCache(),
//   link: link,
// })
import ScrollRestoration from "../components/ScrollRestoration/ScrollRestoration";
const App: React.FC = () => {
  const { tokenRefreshing, tokenVerifying } = useAuth();
  const state = useSelector((state:any) => state.Array.open)
  // useEffect(() => {
  //   console.log("NAVBAR OPEN_____",state);
  // }, [])
 
  console.log("NAVBAR OPEN_____",state);
  if (tokenRefreshing || tokenVerifying) {
    return <Loader/>;
  }

  return (
    
    <Router>  
      <ScrollRestoration />
    <ShopProvider>
      <OverlayProvider>
        <MetaConsumer />
        {demoMode && <DemoBanner />}
        <header>
         {state && state === true ?<Navbar/>: <MainMenu />} 
        </header>
        <Switch>
          <Route exact path={paths.baseUrl} component={HomePage} />  
          <Route exact path={paths.testPage} component={Test1} />  
          <Route exact path={paths.automationPage} component={View} />
          <Route exact path={paths.aboutPage} component={About} />  
          <Route path={paths.autoPage} component={AutomationPage} />
          <Route path={paths.searchUrl} component={SearchPage} />
          <Route path={paths.categoryUrl} component={CategoryPage} />
          <Route path={paths.collectionUrl} component={CollectionPage} />       
          <Route path={paths.productUrl} component={ProductPage} />  
          <Route path={paths.cartUrl} component={CartPage} />
          <Route path={paths.checkoutLoginUrl} component={CheckoutLogin} />
          <Route path={paths.pageUrl} component={ArticlePage} />
          <Route path={accountPaths.baseUrl} component={UserAccount} />
          <Route path={accountPaths.userOrderDetailsUrl} component={OrderDetails} />
          <Route path={paths.guestOrderDetailsUrl} component={OrderDetails} />
          <Route path={paths.accountUrl} component={Account} />
          <Route path={paths.accountConfirmUrl} component={AccountConfirm} />
          <Route path={paths.orderHistoryUrl} component={Account} />
          <Route path={paths.addressBookUrl} component={Account} />
          <Route path={paths.passwordResetUrl} component={PasswordReset} />
          <Route path={paths.checkoutUrl} component={CheckoutPage} />
          <Route path={paths.associates} component={Associates} />
          {<Route path={paths.orderFinalizedUrl} component={ThankYouPage} />}
          <Route component={NotFound} />
          
        </Switch>
        {/* <Routes /> */}
        <Footer />
        <Copy/>
        {/* <Jerry/> */}
        <OverlayManager />
        <Notifications />
      </OverlayProvider>
    </ShopProvider>
    
    </Router>
  );
};

export default App;
